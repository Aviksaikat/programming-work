//select elements

const iconElement = document.querySelector(".weather-icon");
const tempElement = document.querySelector(".temperature-value p");
const descElement = document.querySelector(".temperature-description p");
const locationElement = document.querySelector(".location p");//p for paragraph
const notificationElement = document.querySelector(".notification");

//App data
const weather = {};//weather obj.

weather.temperature =
{
    unit : "celsius"
}

//App const
const KELVIN = 273;
const key = "5fb5bd772854eb9bb1206c1346ae65eb";

//check if the browser supports GeoLocation

if("geolocation" in navigator)
{
    navigator.geolocation.getCurrentPosition(setPosition, showError);
}
else 
{
    notificationElement.style.display = "block";
    notificationElement.innerHTML = "<p>Browser doesn't Support Geolocation</P>";
}

//Set user's position
function setPosition(position)//position form teh API
{
    let latitude = position.coords.latitude;
    let longitude = position.coords.longitude;

    getWeather(latitude,longitude);
}

//Show error
function showError(error)
{
    notificationElement.style.display = "block";
    //notificationElement.innerHTML = "<p>"+error.message+"</p>";//both are same have to use ` to make the output like php or kotlin
    notificationElement.innerHTML = `<p>${error.message}</p>`; 
}
//Data form the API
function getWeather(latitude,longitude)
{
    let api = `http://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=${key}`;

    console.log(api);

    fetch(api)
        .then(function(response)
        {
            let data = response.json();
            return data;
        })
        .then(function (data)
        {
            weather.temperature.value = Math.floor(data.main.temp - KELVIN);
            weather.description = data.weather[0].description;
            weather.iconId = data.weather[0].icon;
            weather.city = data.name;
            weather.country = data.sys.country;
        })
        .then(function()
        {
            displayWeather();
        });
}

//Display Weather UI
function displayWeather()
{
    iconElement.innerHTML = `<img src="icons/${weather.iconId}.png"/>`;
    tempElement.innerHTML = `${weather.temperature.value}°<span>C</span>`;
    descElement.innerHTML = weather.description;
    locationElement.innerHTML = `${weather.city}, ${weather.country}`;
}

//Temp covnt.
function celsiusToF(temperature)
{
    return (temperature * 9/5) + 32;
}

//User click on temp
tempElement.addEventListener("click",function()
{
    if(weather.temperature.value === undefined)
        return;
    if(weather.temperature.unit == "celsius")
    {
        let F = celsiusToF(weather.temperature.value);
        F = Math.floor(F);

        tempElement.innerHTML = `${F}°<span>F</span>`;
        weather.temperature.unit = "fahrenheit";
    }
    else
    {
        tempElement.innerHTML = `${weather.temperature.value}°<span>C</span>`;
        weather.temperature.unit = "celsius";
    }
});

