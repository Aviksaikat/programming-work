var cols = 5;
var rows = 5;
var grid = new Array(cols);

//array
var openSet = []
var closedSet = []
var start;
var end;
var w,h;//height,width each cell/pixel
//alert("Hello! I am an alert box!!");

function Node(i,j)//class
{
    this.x = x;
    this.y = y;
    this.f = 0;//f,g,h val for the algo
    this.g = 0;
    this.h = 0;

    this.show = function ()
    {
        fill(col);
        noStroke();
        rect(this.x*w,this.y*h);


    }
}



//grid setup
function setup() 
{
    createCanvas(400, 400);
    console.log("A*");
    w = width/cols;
    h = height/cols;
    
    //making 2D array
    for(var i=0;i<cols;i++)
    {
        grid[i] = new Array(rows);
    }
    
    for(var i=0;i<cols;i++)
    {
        for(var j=0;j<rows;j++)
        {
            grid[i][j] = new Node(i,j);
        }
    }

    start = grid[0][0];//first point on the canvas
    end = grid[cols-1][rows-1];//last point
    
    openSet.push(start);
    
    
    console.log(grid);
}
//animation 
function draw() 
{
    if(openSet.length > 0)
    {
        //keep going
    }
    else 
    {
        //no sol.
    }
    background(0);

    for(var i=0;i<cols;i++)
    {
        for(j=0;j<rows;j++)
        {
            grid[i][j].show(color(255));
        }
    }
    for(var i=0;i<closedSet.length;i++)
    {
        closedSet[i].show(color(255,0,0));
    }
    for(var j=0;j<openSet.length;j++)
    {
        openSet[i].show(color(0,255,0));
    }

}