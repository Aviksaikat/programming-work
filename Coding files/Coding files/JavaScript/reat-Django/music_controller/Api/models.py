from django.db import models
import string,random

#?table is a model in django
# Create your models here.
def gen_unique_code():
    length = 6

    while True:
        #*gen a random stirng for the room identifier
        code = ''.join(random.choices(string.ascii_uppercase, k=length))
        #! checking if the generated code is unique or not
        if(Room.objects.filter(code=code).count()) == 0:
            break
    return code

class Room(models.Model):
    #*code to identify the room(which room palying what music)
    code = models.CharField(max_length=8, default=gen_unique_code, unique=True)
    host = models.CharField(max_length=50, unique=True)#! one host can have 1 room so unique
    guest_can_pause = models.BooleanField(null=False, default=False)
    votes_to_skip = models.IntegerField(null=False, default=1)
    created_at = models.DateTimeField(auto_now_add=True)


