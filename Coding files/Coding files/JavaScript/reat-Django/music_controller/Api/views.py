from django.shortcuts import render
from rest_framework import generics,status
from .serializers import RoomSerializer,CreateRoomSerializer
from .models import Room
from rest_framework.views import APIView
from rest_framework.response import Response

# Create your views here.
"""
def main(request):
    #*show when we hit this endpoint i.e. when we request this url
    return HttpResponse("<h1>Hellow</h1>")
"""
class RoomView(generics.CreateAPIView):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer
#*APIView helps to override default methods like get,post etc.

class CreateRoomView(APIView):
    serializer_class = CreateRoomSerializer
    
    def post(self,request,format=None):
        #* if the user doesn't have any session then create one
        if(not self.request.session.exists(self.request.session.session_key)):
            self.request.session.create()

        serializer = self.serializer_class(data=request.data)
        #* if the room already created by the user then update the room settings & return the original room
        if(serializer.is_valid()):
            guest_can_pause = serializer.data.get("guest_can_pause")
            votes_to_skip = serializer.data.get("votes_to_skip")
            host = self.request.session.session_key
            queryset = Room.objects.filter(host=host)
            if(queryset.exists()):
                room = queryset[0]
                room.guest_can_pause = guest_can_pause
                room.votes_to_skip = votes_to_skip
                room.save(update_fields=["guest_can_pause", "votes_to_skip"])

            else:
                room = Room(host=host,guest_can_pause=guest_can_pause,votes_to_skip=votes_to_skip)
                room.save()

            return Response(RoomSerializer(room).data, status=status.HTTP_200_OK)                 
