
#! this file translates the backend data into json for front end
from rest_framework import serializers
from .models import Room

#?when handling multiple requests use serializers

class RoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = (  "id","code","host","guest_can_pause",
                    "votes_to_skip","created_at")

class CreateRoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields =("guest_can_pause","votes_to_skip")
        