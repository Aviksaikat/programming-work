from django.urls import path
from .views import RoomView

urlpatterns = [
    #* if we get a blank url call the main fn. form views file 
    path('room',RoomView.as_view()),
]
