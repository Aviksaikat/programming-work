from django.shortcuts import render

# Create your views here.
def index(request, *args, **kwargs):
    #? render the index.html & react will take care of it
    return render(request, "frontend/index.html")