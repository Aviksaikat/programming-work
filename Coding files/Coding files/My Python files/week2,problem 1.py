# Paste your code into this box

balance =42
monthlyPaymentRate = 0.04
annualInterestRate = 0.2
for i in range(12):
    balance = balance - (balance * monthlyPaymentRate) + ((balance - (balance * monthlyPaymentRate)) * (annualInterestRate/12))

print("Remaining balance:", round(balance, 2))