import re#import module re

h = open("regex-sum.txt")#opening the file handle
sum = 0
for line in h:#iterating through each line
    x = line.strip()#remvoving spaces
    f = re.findall("[0-9]+",x)#finding digits 
    #print(f)
    if(len(f)!=0):#filter out empty elements
        for i in f:#interating the list of digits extracted from each line
            sum+= int(i)#type casting and adding the digits
        #print("f=",f)
print("Sum: ",sum)
