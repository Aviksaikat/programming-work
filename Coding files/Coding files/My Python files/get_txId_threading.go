package main

import (
	"fmt"
	// "github.com/PuerkitoBio/goquery"
	"github.com/chromedp/cdproto/page"
	"github.com/chromedp/chromedp"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"sync"
	"context"
	"time"
)

const (
	baseURL = "http://www.techtron.net:4559/Reports/CSP_StudentChallanRcpt.aspx?CollegeId=8&SessionId=13&TranId=%06d&Stu=1"
	minID   = 900000
	maxID   = 117234
)

func main() {
	var wg sync.WaitGroup
	ch := make(chan string, 1000)

	for i := minID; i <= maxID; i++ {
		wg.Add(1)
		go func(id int) {
			defer wg.Done()
			url := fmt.Sprintf(baseURL, id)
			resp, err := http.Get(url)
			if err != nil {
				log.Printf("Error fetching %s: %v\n", url, err)
				return
			}
			defer resp.Body.Close()

			body, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				log.Printf("Error reading response body for %s: %v\n", url, err)
				return
			}

			if strings.Contains(strings.ToLower(string(body)), "saikat") {
				ch <- url
			}
			time.Sleep(5 * time.Second) // Sleep for 5 seconds
		}(i)
	}

	var screenshotWG sync.WaitGroup
	for url := range ch {
		screenshotWG.Add(1)
		go func(url string) {
			defer screenshotWG.Done()

			opts := append(chromedp.DefaultExecAllocatorOptions[:],
				chromedp.Flag("headless", true),
				chromedp.Flag("disable-gpu", true),
				chromedp.Flag("no-sandbox", true),
				chromedp.Flag("ignore-certificate-errors", true),
				chromedp.Flag("enable-logging", false),
				chromedp.Flag("disable-extensions", true),
				chromedp.Flag("mute-audio", true),
			)
			actx, cancel := chromedp.NewExecAllocator(context.Background(), opts...)
			defer cancel()

			ctx, cancel := chromedp.NewContext(actx)
			defer cancel()

			var buf []byte
			if err := chromedp.Run(ctx,
				chromedp.Navigate(url),
				chromedp.ActionFunc(func(ctx context.Context) error {
					var err error
					buf, err = page.CaptureScreenshot().WithQuality(90).Do(ctx)
					return err
				}),
			); err != nil {
				// log.Printf("Error taking screenshot for %s: %v\n", url, err)
				return
			}

			filename := fmt.Sprintf("/home/avik/Downloads/images/%s.png", url[strings.Index(url, "TranId=")+7:])
			if err := ioutil.WriteFile(filename, buf, 0644); err != nil {
				log.Printf("Error writing screenshot file for %s: %v\n", url, err)
				return
			}
			log.Printf("File saved: %s\n", filename)
		}(url)
	}

	wg.Wait()
	close(ch)
	screenshotWG.Wait()
}
