

def dict_invert(d):
    '''
    d: dict
    Returns an inverted dictionary according to the instructions above
    '''
    inverse_dict = []
    
    for k in range(d):
        inverse_dict.append(k[::-1])
    d[:] = inverse_dict(k[::-1])

    return inverse_dict