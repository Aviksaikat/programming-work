import webbrowser
import time
import random
import re


def web_site_visiotr(match):
    while True:
        sites = random.choice([match])
        visit = "https://{}".format(sites)
        webbrowser.open(visit)
        seconds = random.randrange(1, 20)
        time.sleep(seconds)


with open("links.txt", "r") as f:
    for line in f:
        match = re.search(r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', line)
        web_site_visiotr(str(match))
        print(match)

f.close()
