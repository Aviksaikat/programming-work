#!/usr/bin/env python3 
#!convert a list of binary to equivalent text 


l = ["01101000", "01100001", "01110000", "01110000", "01111001"]

res = ''.join([chr(int(x, 2)) for x in l])

print(res)