
def paglafib(x):
    """
    assumes x is an int >= 0
    return fibonqci of x
    """
    if x == 0 or x == 1:
        return 1
    else:
        return paglafib(x-1) + paglafib(x-2)
    