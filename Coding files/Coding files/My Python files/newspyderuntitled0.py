# -*- coding: utf-8 -*-
"""
Created on Fri Jun 22 23:36:39 2018

@author: Avik
"""

def normalize(numbers):
    max_number = max(numbers)
    for i in range(len(numbers)):
        numbers[i] /= float(max_number)
    return numbers  

