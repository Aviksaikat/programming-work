#!/usr/bin/python3
#*generate QR
import pyqrcode

qr = pyqrcode.create("hellow")
qr.png("py.png",scale=8)

#*read the qr
from os import argv
from pyzbar.pyzbar import decode 
from PIL import Image

d = decode(Image.open("qr.png"))
print(d)