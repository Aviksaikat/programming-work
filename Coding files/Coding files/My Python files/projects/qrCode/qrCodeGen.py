#!/usr/bin/python3
#*this is a QR code generator program
import qrcode

qr = qrcode.QRCode(version=2,error_correction=qrcode.constants.ERROR_CORRECT_H,
     box_size=100,border=10)

data = "It's a QR code"
qr.add_data(data)
qr.make(fit=True)
img = qr.make_image(fill="black",back_color="white")
img.save("qr.png")  