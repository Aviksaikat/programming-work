import nltk
from nltk.stem.lancaster import LancasterStemmer
import numpy as np
import tflearn
import random
import json
#import tensorflow as tf
import tensorflow.compat.v1 as tf
import pickle


with open("intents.json") as file:
    data = json.load(file)
#print(data)

#!if the model is trained don't do it again & again
try:
    #*if data exists open & store them to the vars
    with open("data.pickle", "rb") as f:
        words,labels,training,output = pickle.load()

except:
    words = []
    labels = []
    docs_x = []
    docs_y = []
    stemmer = LancasterStemmer()

    for intent in data["intents"]:
        for pattern in intent["patterns"]:
        #*stemming like who's there? -> who's there(focus on main meaning)
            wrds = nltk.word_tokenize(pattern)
            words.extend(wrds)
            docs_x.append(wrds)
            docs_y.append(intent["tag"])
        
        if(intent["tag"] not in labels):
            labels.append(intent["tag"])

    # print(words)
    # print('-' * 50)
    # print(labels)
    # print('-' * 50)
    # print(docs)

    words = [stemmer.stem(w.lower()) for w in words if w != '?']
    #* set to filter uniques
    words = sorted(list(set(words))) 
    labels = sorted(labels)

    #? neural network understands only nums.

    training = []
    output = []

    out_empty = [0 for _ in range(len(labels))]

    for x,doc in enumerate(docs_x):
        bag = []

        wrds = [stemmer.stem(w) for w in doc]

        for w in words:
            if(w in wrds):
                #* 1 the word exist else 0 
                bag.append(1)
            else:
                bag.append(0) 

        output_row = out_empty[:]
        output_row[labels.index(docs_y[x])] = 1 

        training.append(bag)
        output.append(output_row)

    training = np.array(training)
    output = np.array(output)

    #*save data
    with open("data.pickle", "wb") as f:
        pickle.dump((words, labels, training, output), f)

tf.reset_default_graph()

#* the should expect a len of the words we have
net = tflearn.input_data(shape=[None, len(training[0])])
#* no. of neurons 8
net = tflearn.fully_connected(net,8)
net = tflearn.fully_connected(net,8)
#* get probabilites of each output (softmax)
net = tflearn.fully_connected(net, len(output[0]), activation="softmax")
net = tflearn.regression(net)

#? train the model 
model = tflearn.DNN(net)

try:
    model.load("model.tflearn")

except:
    #*no. time the model sees the data more the better
    model.fit(training,output,n_epoch=2000,batch_size=8,show_metric=True)
    model.save("model.tflearn")
    
#* take user input & load the words 
def bag_of_words(s,words):
    bag = [0 for _ in range(len(words))]

    s_words = nltk.word_tokenize(s)
    s_words = [stemmer.stem(word.lower()) for word in s_words]

    for se in s_words:
        for i,w in enumerate(words):
            #*if the current word is in the sentence 
            if(w==se):
                bag[i] = 1

    return np.array(bag)


def chat():

    baje_response = ["Disturb korish na Vag akhan theike","geli","vag gandu",
                    "ja na ai khan theike","ato paji kan"]
    
    print("Ki hoihce bol!!")
    while True:
        inp = input("You: ")
        if(inp.lower() == "quit"):
            break
        
        #*probability .pick the first
        results = model.predict([bag_of_words(inp,words)])[0]
        #*give the index of the greatest value from the list
        results_index = np.argmax(results)
        tag = labels[results_index]
        
        if(results[results_index] > 0.75):
            for tg in data["intents"]:
                if(tg["tag"] == tag):
                    responses = tg["responses"]
            
            print(random.choice(responses))
        else:
            print(random.choice(baje_response))

chat()