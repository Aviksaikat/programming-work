from flask import Flask
from flask_restful import Api, Resource, reqparse, abort, fields, marshal_with
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
api = Api(app)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///database.db"
db = SQLAlchemy(app)

class VideoModel(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100),nullable=False)#nullable bcz every YT video should have name always
    views = db.Column(db.Integer,nullable=False)
    likes = db.Column(db.Integer,nullable=False)

    def __repr__(self):
        return f"Video( name = {name}, views = {views}, likes = {likes})"

#* 1st time only
#! db.create_all() 

#for insert
video_put_args = reqparse.RequestParser()
video_put_args.add_argument("name", type=str, help="Name of the video is required", required=True)
video_put_args.add_argument("views", type=int, help="Views of the video", required=True)
video_put_args.add_argument("likes", type=int, help="Likes on the video", required=True)

#for update
video_update_args = reqparse.RequestParser()
video_update_args.add_argument("name", type=str, help="Name of the video is required")
video_update_args.add_argument("views", type=int, help="Views of the video")
video_update_args.add_argument("likes", type=int, help="Likes on the video")

resource_fields = {
	"id": fields.Integer,
	"name": fields.String,
	"views": fields.Integer,
	"likes": fields.Integer
}

"""
#stop if video id not exisit
def abort_invalid_id(video_id):
    if(video_id not in videos):
        abort(404, message="Video id is not valid....")

#stop if video already exisits
def abort_video_exists(video_id):
    if(video_id in videos):
        abort(409,message="Video already exists with that ID....")
"""

class Video(Resource):
    @marshal_with(resource_fields) #takes the return value & serialize
    def get(self,video_id):
        result = VideoModel.query.filter_by(id=video_id).first()
        if not result:
            abort(404, message="Could not find video with that id")
        return result
    
    @marshal_with(resource_fields)
    def put(self,video_id):
        args = video_put_args.parse_args()
        result = VideoModel.query.filter_by(id=video_id).first()
        if result:
            abort(409, message="Video id istaken....")        
        
        video = VideoModel(id=video_id, name=args["id"], views=args["views"], likes=args["likes"])
        db.session.add(video)
        db.session.commit()#permanently addig
        return video, 201
    
    @marshal_with(resource_fields)
    def patch(self,video_id): #update method 
        args = video_update_args.parse_args()
        result = VideoModel.query.filter_by(id=video_id).first()
        if not result:
            abort(404, message="Video doesn't exist,cannot update")
        
        if args["name"]:
            result.name = args["name"]
        if args["views"]:
            result.name = args["views"]
        if args["likes"]:
            result.name = args["likes"]

        db.session.add(result)
        db.session.commit()
        return result
    
    def delete(self, video_id):
        args = video_update_args.parse_args()
        result = VideoModel.query.filter_by(id=video_id).first()
        if not result:
            abort(404, message="Video doesn't exist,cannot delete")
        
        del result["name"]  
        return "", 204


#api.add_resource(HellowWorld, "/hellow/<string:name>/<int:age>")#<> to give parameters
api.add_resource(Video,"/video/<int:video_id>")

if __name__  == "__main__":
    app.run(debug=True) #open in debug mode for testing 

