import requests
import json
from tqdm import tqdm #for progess bar
#*gets all the data of a channel from youtube everything!!!!

class YTstats(object):
    def __init__(self,api_key,channel_id):
        self.api_key = api_key
        self.channel_id = channel_id
        self.channel_statistics = None
        self.vid_data = None

    def get_channel_statistics(self):
        url = f"https://www.googleapis.com/youtube/v3/channels?part=statistics&id={self.channel_id}&key={self.api_key}"
        #print(url)
        json_url = requests.get(url)
        data = json.loads(json_url.text)
        #print(data)
        try:
            data = data["items"][0]["statistics"]
        except KeyError:
            data = None
        self.channel_statistics = data
        return data

    def get_channel_vid_data(self):
        #1.vid id
        channel_vids = self._get_channel_vid(limit=50)
        print(len(channel_vids))
        #2.vid stat
        parts = ["snippet","statistics","contentDetails"]
        for vid_id in tqdm(channel_vids):
            for part in parts:
                data = self._get_single_vid_data(vid_id,part)
                channel_vids[vid_id].update(data)

        self.vid_data = channel_vids
        return channel_vids


    def _get_single_vid_data(self,vid_id,part):
        url = f"https://www.googleapis.com/youtube/v3/videos?part={part}&id={vid_id}&key={self.api_key}"         
        json_url = requests.get(url)
        data = json.loads(json_url.text)#dict 
        try:
            data = data["items"][0][part]
        except:
            print("Parsing error")
            data = {}
        return data


    def _get_channel_vid(self, limit=None):
        #print("Limit= ",limit)
        url = f"https://www.googleapis.com/youtube/v3/search?key={self.api_key}&channelId={self.channel_id}&part=id&order=date"
        if(limit is not None and isinstance(limit,int)):
            url +="&maxResults=" + str(limit)
        #print(url)
        #video & next page token
        vid, npt = self._get_channel_vids_per_page(url)
        idx = 0 #* to limit the API call in case of error
        while(npt and idx<10):
            next_url = url + "&pageToken=" +npt
            next_vid , npt = self._get_channel_vids_per_page(next_url)
            vid.update(next_vid)
            idx+=1

        return vid

        
    def _get_channel_vids_per_page(self,url):
        json_url = requests.get(url)
        data = json.loads(json_url.text)#converting to json
        channel_vids = {}
        if("items" not in data):
            return channel_vids, None
        
        item_data = data["items"]
        next_page_token = data.get("nextPageToken", None)#if not found return None
        for item in item_data:
            try:
                kind = item["id"]["kind"]
                if(kind == "youtube#video"):
                    vid_id = item["id"]["videoId"]
                    channel_vids[vid_id] = {}
            except KeyError:
                print("No data in id")
        return channel_vids, next_page_token
        

    
    def data_dump(self):
        if(not self.channel_statistics or not self.vid_data):
            print("Data is None")
            return 
        
        fused_data  = {self.channel_id: {"channel_stat":self.channel_statistics,"vid_data":self.vid_data}}
        
        channel_title = self.vid_data.popitem()[1].get("channelTitle",self.channel_id)#returns tuple
        channel_title.replace(" ","_").lower()
        file_name = channel_title + ".json"
        with open(file_name,'w') as f:
            json.dump(fused_data, f ,indent=4)
        
        print("file dumped")




    