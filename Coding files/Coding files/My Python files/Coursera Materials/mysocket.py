import socket
import requests#to get the Http header

my_sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
my_sock.connect(("data.pr4e.org",80))
cmd = "GET http://data.pr4e.org/romeo.txt HTTP/1.0\n\n".encode()
my_sock.send(cmd)

while True:
    data = my_sock.recv(512)#512 char length
    if(len(data)<1):
        break #end of file
    print(data.decode())
my_sock.close()
print(requests.get(url="http://data.pr4e.org/intro-short.txt").headers)