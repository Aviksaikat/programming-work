import urllib.request, urllib.parse, urllib.error
from urllib.request import urlopen
from bs4 import BeautifulSoup
import ssl

ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

url = input("Enter url - ")
html = urllib.request.urlopen(url, context=ctx).read()
soup = BeautifulSoup(html, 'html.parser')
tags = soup("span")#search for span tags
l = []
for tag in tags:
    #print("Type of tag = ",type(tags))
    #print("Hi")
    #print("Tag",tag)
    span_tag = tag.contents[0]#list of contents
    #if (span_tag is not None):
    l.append(span_tag)
#print(l)
summ=0
for i in l:
    summ+=int(i)

print("Count ",len(l))
print("Sum ",summ)