import urllib.request, urllib.parse, urllib.error
import xml.etree.ElementTree as ET
import ssl

#api_key = False
# If you have a Google Places API key, enter it here
# api_key = 'AIzaSy___IDByT70'
# https://developers.google.com/maps/documentation/geocoding/intro
"""
if api_key is False:
    api_key = 42
    serviceurl = "http://py4e-data.dr-chuck.net/xml?"
else :
    serviceurl = "https://maps.googleapis.com/maps/api/geocode/xml?"
"""
"""don't need to save data anywhere.so don't need above codes"""
# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

while True:
    url = input("Enter location: ")
    if len(url) < 1: 
        break
    print("Retreiving ",url)
    uh = urllib.request.urlopen(url,context = ctx)
    data = uh.read()
    print("Retreived",len(data),"characters")#formality to show no. of chars returned for request
    #print(data.decode())
    tree = ET.fromstring(data)#forming the tree 
    summ = 0
    counts = tree.findall(".//count")#.//find all occurrence irrespective of pos
    ct1 = counts[0].text
    print("Count:",ct1)
    for i in range(len(counts)):
        ct2 = counts[i].text
        summ+=int(ct2)
    #print(counts)
    print("Sum:",summ)

