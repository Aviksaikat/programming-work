import urllib.request, urllib.parse, urllib.error
import ssl,json
# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

while True:
    url = input("Enter location: ")
    if(len(url) < 1):
        break
    print("Retreving ",url)
    uh = urllib.request.urlopen(url,context=ctx)
    data = uh.read()
    print("Retreived",len(data),"characters")
    #print(data.decode())
    info = json.loads(data)
    summ = 0
    #print(info)
    #print(json.dumps(info, indent=2))
    print("Count: ",info["comments"][1]["count"])#dict inside a list inside another dict  
    #syntax is dict list dict
    for item in info["comments"]:
        #print("Count: ",item["count"])
        summ+= int(item["count"])
    print("Sum:",summ)