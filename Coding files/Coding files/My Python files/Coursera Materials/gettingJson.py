import urllib.request, urllib.parse, urllib.error
import xml.etree.ElementTree as ET
import ssl,json
# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE


url = input("Enter location: ")
uh = urllib.request.urlopen(url,context=ctx)
connection = urllib.request.urlopen(url, context=ctx)
data = connection.read().decode()

js = json.loads(data)
print(json.dumps(js, indent=2))

info = json.loads(data)#info is list here as [] present in the string data
print('User count:', len(info))

