#!/usr/bin/python3
#! FIFO
#*my defiened class
from exceptions import Empty

#? Queue class
class Queue():
    def __init__(self):
        self.data = []
        self.size = 0
        self.front = 0
    
    def __len__(self):
        return self.size
    
    #*return true if the len is 0
    def isEmpty(self):
        return len(self.data) == 0
    
    #*insert
    def enqueue(self,e):
        self.data.append(e)
        self.size+=1
    
    #*delete
    def deque(self):
        if self.isEmpty():
            raise Empty("Queue is Empty")
        value = self.data[self.front]
        self.data[self.front] = None        
        self.front+=1
        self.size-=1
        return value
    
    #*first element of the queue
    def first(self):
        if self.isEmpty():
            raise Empty("Queue is Empty")
        return self.data[self.front]


q = Queue()
q.enqueue(10)
q.enqueue(60)
q.enqueue(50)
print("Queue: ",q.data)
print("Len: ",len(q))
print("First Element: ",q.first())
print("Deleted: ",q.deque())
print("Queue: ",q.data)
print("Deleted: ",q.deque())
print("Queue: ",q.data)
q.enqueue(500)
print("Deleted: ",q.deque())
print("Len: ",len(q))
print("Queue: ",q.data)
