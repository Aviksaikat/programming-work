#!/usr/bin/python3
#! LIFO
#*my defiened class
from exceptions import Empty

#? Stack class
class Stack():
    #*constructor
    def __init__(self):
        self.data = []
    
    #*overriding the defiend len fn.
    def __len__(self):
        return len(self.data)
    
    #*checking stack is empty or not
    #*return true if the len is 0
    def is_empty(self):
        return len(self.data) == 0

    #*pushing elements in stack
    def push(self,e):
        self.data.append(e)
    
    #*poping elements in stack
    def pop(self):
        #! self. bcz we're using this method on the given particular obj.
        if(self.is_empty()):
            raise Empty("Stack is Empty")
        return self.data.pop()
    
    #*returning the top value
    def top(self):
        if(self.is_empty()):
            raise Empty("Stack is Empty")
        return self.data[-1]

s = Stack()
s.push(10)
s.push(50)
s.top()
print(s.data)
print("Length:",len(s))
print("Pop",s.pop())
print(s.data)
print("Pop",s.pop())
s.push(10283)
s.push(500)
s.push(10)
s.push(50)
print(s.data)
print("Pop",s.pop())