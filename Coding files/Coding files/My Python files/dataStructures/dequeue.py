#!/usr/bin/python3
#!Deque insert & delete can de done form both side(front & rear)
#*my defined class
from exceptions import Empty

#? Dequeue class
class Dequeue():
    def __init__(self):
        self.data = []
        self.front = 0
    
    def __len__(self):
        return len(self.data)

    #*return true if the len is 0
    def is_empty(self):
        return len(self.data) == 0
    
    def first(self):
        if self.is_empty():
            raise Empty("Dequeue is Empty")
        return self.data[self.front]
    
    #*insert bcz we want to inset the data where the current front is located
    def add_first(self,e):
        self.data.insert(self.front,e)
    
    #*appned adds at last
    def add_last(self,e):
        self.data.append(e)
    
    def delete_first(self):
        if self.is_empty():
            raise Empty("Dequeue is Empty")
        return self.data.pop(self.front)
    
    #*delete the last element so pop
    def delete_last(self):
        if self.is_empty():
            raise Empty("Dequeue is Empty")
        return self.data.pop()
    
d = Dequeue()
d.add_first(10)
d.add_first(20)
d.add_last(69)
d.add_first(12)
d.add_first(46)
d.add_first(34)
d.add_last(39)
d.add_first(56)
print("Len: ",len(d))
print(d.data)
print("Deleted from first: ",d.delete_first())
print(d.data)
print("Deleted from last: ",d.delete_last())
print("Len: ",len(d))
print(d.data)