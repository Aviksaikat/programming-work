# -*- coding: utf-8 -*-
"""
Created on Fri Oct 18 18:04:56 2019

@author: Avik
"""
import time 
def is_prime(n): 
    if n <= 1: 
        return False
    for i in range(2,n): 
        if n % i == 0: 
            return False
    return True
  
# Driver function 
t0 = time.time() 
c = 0 #for counting 
n=2 
while(c!=10001): 
    x = is_prime(n) 
    #print(n)
    c += x
    n+=1
print("Total prime numbers in range :", c) 
print(c,"th prime is",n-1)
t1 = time.time() 
print("Time required :", t1 - t0) 
    