#!/usr/bin/env python3
#? decorators

def func(f):
    #! *args any no. of arguments, **kwargs ignore keyword arguments
    def wrapper(*args,**kwargs):
        print("Started")
        #*calling the passed fn.
        rv = f(*args,**kwargs)
        print("Ended")
        #* to return from the fn.s
        return rv 

    return wrapper

@func #!same as func2 = func(func2)
def func2(x,y):
    print("It's Func 2")
    #print(x,y)
    return x 

#x = func(func2)
#print(x)
#* we're returning the function in func & storing it to x not calling
#* & then calling it  
#x()
x = func2(10,13)
print("Returnred:",x)