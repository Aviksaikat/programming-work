from collections import Counter
from collections import namedtuple
from collections import deque #deck

c = Counter("Sikat Karmakar") #case sensitive
d = Counter(['a','i','k','t'])
c.update(d) # not add it's update for Counter
print(c)
print("After c+d = ",c+d)
print("After c-d = ",c-d-d)#if 0 will not be shown after operation
print("After c intersection d = ",c & d)
print("After c union d = ",c | d)

point = namedtuple("point","x y z")
newP = point(3,4,5)
print(newP)
point2 = namedtuple("point2",['x','y', 'z'])
#in case of dict it takes only the keys ignoring the values 
newP = point2(10,20,3)
print(newP)
print("newP.x =",newP.x,", newP[0] =",newP[0])
point3 = namedtuple("point3",{'x':0,'y':0,'z':0 })
newP = point3(1,2,3)
print("newP.__asdict() = ",newP._asdict())

d = deque("hellow")
print(d)
d.append(4)
d.append("yo")
print(d)
d.appendleft("hagu")
print(d)
d.pop()
print(d)
d.popleft()
print(d)
d.clear()
print(d)
l = ["456",12,True]
d.extend(l)
print(d)
d.extend("hellow")
print(d)
d.extendleft("abarHagu")#adds backwards
print(d)
d.rotate(1)
print(d)
d.rotate(-1)
print(d)
d.clear()
print(d)
d = deque("hellow",maxlen=5)
print(d)
d.append(10)
print(d)
for i in d:
    print(i,end = " ")
d.reverse()

print("\n",d)