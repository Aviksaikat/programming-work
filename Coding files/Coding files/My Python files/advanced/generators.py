#!/usr/bin/env python3
#? generators

class Gen():
    def __init__(self,n):
        self.n = n
        self.last = n
    
    def __next__(self):
        return self.next()
    
    def next(self):
        if(self.last == self.n):
            raise StopIteration

        rv = self.last ** 2
        self.last+=1
        return rv
    
g = Gen(1000000)

while True:
    try:
        print(next(g))

    except StopIteration:
        break


def gen(n):
    for i in range(n):
        #*yield pauses the exe of the fn. Vs. return stops it
        #*yield resumes where it left off
        yield i**2 

g = gen(1000000)
for i in g:
    print(i)

print(next(g))