#!/usr/bin/python
import time

def timer(func):
    def inSide(*args,**kwargs):
        start = time.time()
        rv = func()
        total = (time.time() - start)
        print("Time taken:",total)
        return rv
    return inSide

@timer
def test():
    for _ in range(100000000):
        pass

test()