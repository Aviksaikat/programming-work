#!/usr/bin/python
#? Dunder method/magic method
"""
[summary]
    By using this method we can do all types of operations 
    on our own object
"""
#*https://docs.python.org/3/reference/datamodel.html

class Person():
    def __init__(self, name):
        self.name = name    
    
    #!dunder method
    def __repr__(self):
        return f"Person({self.name})"
    
    def __mul__(self,x):
        if(type(x) is not int):
            raise Exception("Invalid argument, must be an int")

        self.name = self.name * x

    def __call__(self,y):
        print(y)

    def __len__(self):
        return len(self.name)

p = Person("Saikat")
print(p)
p*4
print(p)
print("Calling p(4)")
p(4)
print("Len of p is:",len(p))