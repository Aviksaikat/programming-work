#!/usr/bin/python
class Foo:
    def show(self):
        print("woo")

def add(self):
    self.z = 10

#! same as defining a class
#? syntax = "class Name",(inheritance),{attributes}
Test = type("Test",(Foo,),{'x':5,"add_attribute":add})

t = Test()
#print(Test)
t.woo = "WoooHoo"
#print(t.x)
#print(t.woo)
t.add_attribute()
#print(t.z)

#*Creating Meta Class
class Meta(type):
    #! __new__ is called before the __init__ i.e. 1st thing is called while instantiating 
    def __new__(self,class_name,bases,attributes):
        print(attributes)

        d = {}
        for name,value in attributes.items():
            if(name.startswith("__")):
                d[name] = value
            else:
                d[name.upper()] = value

        print(d)
        return type(class_name,bases,d)

class Dog(metaclass=Meta):
    x = 10
    y = 15

    def hellow(self):
        print("hi")

d = Dog()
print(d.X)#* X not x bcz we changed the names to capital
