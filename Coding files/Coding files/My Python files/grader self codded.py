from math import tan , pi

def polysum(n,s):
    '''
        A regular polygon has n number of sides. Each side has length s.
        n is a positive integer and n > 0.
    '''
    area = (0.25*n*s*s)/tan(pi/n)
    perimeter = n*s
    
    polysum = (area+(perimeter**2))
    ans = round(polysum,4)
    return ans
    
    
    
    