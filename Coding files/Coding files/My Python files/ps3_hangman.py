# Hangman game
#

# -----------------------------------
# Helper code
# You don't need to understand this helper code,
# but you will have to know how to use the functions
# (so be sure to read the docstrings!)

import random

WORDLIST_FILENAME = "words.txt"

def loadWords():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = line.split()
    print("  ", len(wordlist), "words loaded.")
    return wordlist

def chooseWord(wordlist):
    """
    wordlist (list): list of words (strings)

    Returns a word from wordlist at random
    """
    return random.choice(wordlist)

# end of helper code
# -----------------------------------

# Load the list of words into the variable wordlist
# so that it can be accessed from anywhere in the program
wordlist = loadWords()

def isWordGuessed(secretWord, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: boolean, True if all the letters of secretWord are in lettersGuessed;
      False otherwise
    '''
    # FILL IN YOUR CODE HERE...
    for char in secretWord:
      if char not in lettersGuessed:
        break
        return False
    return True    
        



def getGuessedWord(secretWord, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters and underscores that represents
      what letters in secretWord have been guessed so far.
    '''
    # FILL IN YOUR CODE HERE...
    empty = []

    for i in secretWord:
      if i in lettersGuessed:
        empty.append(i)
      else:
        empty.append("_")
    return " ".join(empty)



import string 
def getAvailableLetters(lettersGuessed):
    '''
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters that represents what letters have not
      yet been guessed.
    '''
    # FILL IN YOUR CODE HERE...
    alphabet = string.ascii_lowercase
    get_string = []


    for i in alphabet:
      if i not in lettersGuessed:
        get_string.append(i)
    return  " ".join(get_string)





def hangman(secretWord):
    '''
    secretWord: string, the secret word to guess.

    Starts up an interactive game of Hangman.

    * At the start of the game, let the user know how many 
      letters the secretWord contains.

    * Ask the user to supply one guess (i.e. letter) per round.

    * The user should receive feedback immediately after each guess 
      about whether their guess appears in the computers word.

    * After each round, you should also display to the user the 
      partially guessed word so far, as well as letters that the 
      user has not yet guessed.

    Follows the other limitations detailed in the problem write-up.
    '''
    # FILL IN YOUR CODE HERE...

    mistakesMade = 0
    num_guess = []
    n = len(secretWord)
    print("Welcome to the game, Hangman!")
    print("I am thinking of a word that is" , n , " letters long.")
   
    
    guessaLetter = input()

    input_in_lower = guessaLetter.lower()
    while lettersGuessed = 0 or < 1:
      break
      if i in secretWord:
        num_guess.append(i)
        print("Good guess:", getGuessedWord)
        if i not in secretWord:
          break
      elif:
        guessaLetter is in secretWord:
        break
      else:
        i not in getGuessedWord:
        num_guess.append(i)
        mistakesMade += 1
        print("Oops! That letter is not in my word:" , getAvailableLetters)
      print("You have " , mistakesMade , "guesses left") 
      print("Available letters:" , getAvailableLetters)
      print("Please guess a letter:" , guessaLetter)
      continue
      if num_guess < 1:
        break
        print("------------")
        print("Congratulations, you won!")
      else:
        num_guess = 0:
        break
        print("------------")
        print("Sorry, you ran out of guesses. The word was " , secretWord)
        










# When you've completed your hangman function, uncomment these two lines
# and run this file to test! (hint: you might want to pick your own
# secretWord while you're testing)
secretWord = chooseWord(wordlist).lower()
hangman(secretWord)
