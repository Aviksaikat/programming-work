import sys
import numpy as np
from PIL import Image
import wikipedia
from wordcloud import WordCloud,STOPWORDS

x = str(input("Enter the title : "))

title=wikipedia.search(x)[0]
page=wikipedia.page(title)
text=page.content
#print(text)

backgroud=np.array(Image.open(r"C:\Users\saiki\OneDrive\Coding files\My Python files\projects\virtualenv\cloud.png"))
stopwords=set(STOPWORDS)#remove words a,the etc.
wc=WordCloud(background_color="white",max_words=200,mask=backgroud,stopwords=stopwords)

wc.generate(text)

wc.to_file(r"C:\Users\saiki\OneDrive\Coding files\My Python files\projects\virtualenv\Word_CLoudprogramming.png")