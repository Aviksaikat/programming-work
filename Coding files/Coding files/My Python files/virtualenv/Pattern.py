import turtle

turtle.bgcolor("black")
turtle.pensize(2)
turtle.speed(0)
b=["red","blue","green","purple","grey"]

for i in range(9):
    for j in b:
        turtle.color(j)
        turtle.circle(100)
        turtle.left(10)
turtle.mainloop()
