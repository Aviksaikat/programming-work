import os

def new_directory(directory, filename):
  # Before creating a new directory, check to see if it already exists
  if os.path.isdir(directory) == False:
    os.mkdir(directory)

  # Create the new file inside of the new directory
  os.chdir(directory)
  with open (filename,'w') as file:
    pass
  #path = os.join()
  list_dir = os.listdir()
  # Return the list of files in the new directory
  return list_dir

print(new_directory("PythonPrograms", "script.py"))