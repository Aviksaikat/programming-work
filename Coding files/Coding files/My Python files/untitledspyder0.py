# -*- coding: utf-8 -*-
"""
Created on Wed Jun 20 14:08:40 2018

@author: Avik
"""


def rem(x, a):
    """
    x: a non-negative integer argument
    a: a positive integer argument

    returns: integer, the remainder when x is divided by a.
    """
    if x == a:
        return 0
    elif x < a:
        return x
    else:
        return rem(x-a, a)