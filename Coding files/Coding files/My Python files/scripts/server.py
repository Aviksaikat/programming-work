#!/usr/bin/python3
# this is a http server using python
import http.server
import socketserver
import socket

PORT = int(input("Enter your desired port: "))

Handler = http.server.SimpleHTTPRequestHandler
ip_addr = socket.gethostbyname(socket.gethostname())
with socketserver.TCPServer(("", PORT), Handler) as httpd:
    #print("Starting server at ",ip_addr,"->",PORT)
    print("http://"+ip_addr+":"+str(PORT))
    #print("serving at port", PORT)
    httpd.serve_forever()