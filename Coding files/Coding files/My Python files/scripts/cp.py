import pyperclip
from pynput.keyboard import Key,Listener
def on_press(key):
    file_write()

def file_write():
    with open("a.txt","a") as f:
        f.write(pyperclip.paste())
        f.write("\n")
    f.close()

def on_release(key):
    if(key == Key.esc):
        return False

with Listener(on_press=on_press,on_release = on_release) as listenes:
    listenes.join()



