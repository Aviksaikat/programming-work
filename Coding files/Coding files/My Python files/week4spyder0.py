
"""
Created on Wed Jun 20 14:08:40 2018

@author: Avik
"""
def genPrimes():
    Primes = []
    lastNum = 1
    while True:
        lastNum += 1
        for p in Primes:
            if lastNum % p  != 0:
                break
            else:
                Primes.append(lastNum)
                yield lastNum
          
    primeGenerator = genPrimes()
   
    primeGenerator.next()

