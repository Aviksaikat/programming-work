import io
import os
from google.cloud import vision
from google.cloud.vision import types
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = r'My Project 1-f90d4552ad5d.json'

client = vision.ImageAnnotatorClient()

FILE_NAME = "22.png"
FOLDER_PATH = r"C:\Users\saiki\Downloads"

with io.open(os.path.join(FOLDER_PATH,FILE_NAME), 'rb') as image_file:
    content = image_file.read()
image = vision.types.Image(content=content)

response = client.text_detection(image=image )