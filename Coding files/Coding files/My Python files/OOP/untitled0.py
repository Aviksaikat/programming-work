# -*- coding: utf-8 -*-
"""
Created on Sat Jun 13 10:37:31 2020

@author: Avik
"""
class Car:
    def __init__(self,max_speed,speed_unit):
        self.max_speed  = max_speed
        self.speed_unit = str(speed_unit)
    def __str__(self):    
        return str("Car with the maximun speed of",self.max_speed,self.speed_unit)
    


class Boat:
    def __init__(self,max_speed):
        self.max_speed  = max_speed
    def __str__(self):
        return str("Boat with the maximum speed of",self.max_speed,"knots")
    
    
j=Car(130,"mph")
#Boat(70,"knots")
print(j)

