# abstraction hides internal details
# only shows functionality
from abc import ABC, abstractclassmethod


class Shape(ABC):  # inheriting from module
    # decorator:allows to add new functionality
    # to an existing object without modifying the object's structure
    @abstractclassmethod #decorators

    def area(self):
        pass

    @abstractclassmethod
    def perimeter(self):
        pass


# subclass
class Square(Shape):  # inheriting
    def __init__(self, side):
        self.__side = side

    def area(self):
        return self.__side * self.__side

    def perimeter(self):
        return 4 * self.__side


# my_shape = Shape()

my_square = Square(5)

print(my_square.area())
print((my_square.perimeter()))
