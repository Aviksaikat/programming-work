class Person:
    def __init__(self, fname, lname):
        """      
        :type fname,lname: object
        """
        self.firstname = fname
        self.lastname = lname

    def printname(self):
        print(self.firstname, self.lastname)


florist = Person("Saikat", "Karmakar")
florist.printname()

"""class Lawyers(Person):
     child class inheriting from parent class Person
    pass
"""


# child class
class Lawyers(Person):
    def __init__(self, fname, lname, casetype):
        """
        :type fname,lname: object
        """
        Person.__init__(self, fname, lname)  ##calling the parent class having access to the methods of the parent class
        ##same as the below 2 lines
        # self.firstname = fname
        # self.lastname = lname
        self.casetype = casetype
        # adding an attribute to the child class

    def printinfo(self):
        print("hellow mera naam ", self.firstname, self.lastname)


# if method name in child and parent are same the attributes of that method of the child class will
# overwrite
happy_lawyers = Lawyers("Pagla", "chulke de", "case jondis")
happy_lawyers = Lawyers("Pagla", "tor", "case jondiss")
happy_lawyers.printinfo()
print(happy_lawyers.casetype)
