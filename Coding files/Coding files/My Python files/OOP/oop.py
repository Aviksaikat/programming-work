#in python class is an object itself
class IstClass:
    companyName = "Yo Yo naam jani na akhono"

    def __init__(self, course, duration):
    # __init__ built in fn. the initializer: used to initialize objects with an initial value
        self.course = course
        # holding valriable
        """self is the reference to the current instance of the class
            used to access variables and methods belonging to the class """
        self.duraion = duration

    def printinfo(self):
        print("My Company name is:", IstClass.companyName)


class Pets:
    pass  # class has not methods


elearning = IstClass("Python python haha best ata", 7)  # instantiated class
bls = IstClass("Django ki jiish re baba", 8)
bls.course = "oporer value changed"

bls.printinfo()

print(bls.course)
# del bls.duration ##deleting attribute next line will cause error
print(bls.duraion)
