# prevents direct(accidental) data modification
# internal representation of an object to be hidden of the outside view of the object's def.
# class is an ex.
# in python __(double underscore) to make a function private

class Cars():
    def __init__(self, speed, color):
        # self.speed = speed
        # self.color = color
        self.__speed = speed
        self.__color = color
        # encapsulating by __

    def set_speed(self, value):
        self.__speed = value

    def get_speed(self):
        return self.__speed

    def set_color(self, value):
        self.__color = value

    def get_color(self):
        return self.__color
    #to access the values after encap.
    #set and get_color is defined

# making objects
ford = Cars(200, "black")
ferrrari = Cars(400, "red")
toyota = Cars(250, "yellow")

ford.set_speed(450)  # after instantiating changing the value of valirable

ford.speed = 500  # accesing the speed variable directly
#useless after encap.
print(ford.get_speed())

#print(ford.color)

#print(ford.__color)#after encapsulating
#same will not make any diff. bcz color is private


print(ford.get_color())

#only way to access after encapsulation