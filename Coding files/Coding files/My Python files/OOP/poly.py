# Polymorphism = ability to take or have various forms
"""
    ex. of Polymorphism
    print(len("hello"))
    >>5
    print(len([12,4,44]))
    >>3
    same func. len gives length of a sting and size of a list as well
"""


# polymorphic fn.

def addNumbers(a, b, c=1):
    return a + b + c


# print(addNumbers(8, 9))
# print(addNumbers(8, 9, 4))


# polymorphic class with methods

class UK():
    def capital_city(self):
        """
        :return type: object
        """
        print("London is the capital of UK")

    def language(self):
        print("English is primary language of UK")


class India():
    def capital_city(self):
        print("Delhi is the capital of India")

    def language(self):
        print("English is not the primary language of india")


def asia(asi):
    asi.capital_city()
    # fn. that takes object as argument


queen = UK()  # instantiate == creating object from class
# queen.capital_city() #adiing method to the instance
zara = India()
# zara.capital_city()
"""
for country in (queen,zara):
    country.capital_city()
    country.language()
"""

asia(queen)  # passing an object to an fn.
asia(zara)
