s="dfqghijgklm"

count = 0
maxcount = 0
result = 0

for p in range(len(s)-2):
    if (s[p] <= s[p + 1]):
        count += 1
        if count > maxcount:
            maxcount = count
            result = p + 1
    else:
        count = 0
startposition = result - maxcount
print('Longest substring in alphabetical order is:', s[startposition:result + 1])
