def gcdIter(a, b):
    '''
    a, b: positive integers
    
    returns: a positive integer, the greatest common divisor of a & b.
    '''
    # Your code here
    testvalue = min(a,b)

    while a % testvalue != 0 or b % testvalue != 0:
        testvalue -=1
        print(a,b,testvalue)
    return testvalue

