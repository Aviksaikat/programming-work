def closest_power(base, num):
    '''
    base: base of the exponential, integer > 1
    num: number you want to be closest to, integer > 0
    Find the integer exponent such that base**exponent is closest to num.
    Note that the base**exponent may be either greater or smaller than num.
    In case of a tie, return the smaller value.
    Returns the exponent.
    '''
    result = 0

    if base > num:
        return 0
    elif base == num:
        return 1
    else:
        for i in range(1, int(num)):
            if abs(base**i - num) <= abs(base**(i+1) - num):
                result = i
                break
    return result










def getSublists(L, n):

    while len(L) >= n:
        L[n].append(yo)
        return yo








        






def dict_invert(d):
    '''
    d: dict
    Returns an inverted dictionary according to the instructions above
    '''

       