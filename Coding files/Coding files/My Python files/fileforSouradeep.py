def transpose(m):
    transposed = [None]*len(m[0])
    for t in range(len(m)):
        transposed[t] = [None]*len(m)
        for tt in range(len(m[t])):
            transposed[t][tt] = m[tt][t]
    print (transposed)