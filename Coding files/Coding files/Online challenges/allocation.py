T = int(input())
j = T
a =[]
l = []
summ=0
def solve():
    n,b = input().split()
    N = int(n)
    B = int(b)
    l = list(map(int,input().split()))
    if(len(l)!=N):
        exit(1)
    l.sort()
    i=0
    global summ 
    summ =0
    for i in l:
        if(B>=i):
            B-=i
            summ+=1
    

k = 1
while(T):
    solve()
    print("Case #"+str(k)+":",summ)
    k+=1
    T-=1
#for competative programming minimizing time complexity is the key