def cal(a):
    c = 0
    d = 0
    while(a>=3):
        a = a/3
        c+=1
        if(not(a.is_integer())):
            d+=1
            a = int(a)
    if(a==2 or d!=0):
        c+=1
    return c
n = int(input())
print(cal(n))