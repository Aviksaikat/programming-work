# -*- coding: utf-8 -*-
"""
Created on Thu Oct 17 14:21:40 2019

@author: Avik
"""
def ispal(x):
    n=x
    i=0
    while(x!=0):
        i*=10
        i=i+x%10
        x=int(x/10)
    if(n==i):
        return 1
    else:
        return 0
l=[]
for i in range(899,1000):
    for j in range(899,i+1):
        z=i*j
        #print("i=",i,"j=",j,z)
        if(ispal(z)):
            l.append(z)
print("Largest palindrome is :",l[-1])
    