import itertools
p,h = input().split() 
L = int(p)
H = int(h)
k = int(input())
l = []
def even(n):
    if(n%2 == 0):
        return 0
    else:
        return 1

i=L
while(i<=H):
    l.append(i)
    i+=1
#print("l ",l)
all_combinations = []
for r in range(k):
    combinations_object = itertools.combinations(l, r)
    combinations_list = list(combinations_object)
    all_combinations += combinations_list
print(all_combinations) 
