

"""
from collections import Counter

i = input()
c = Counter(i) #case sensitive

print(c.most_common())
print(c.most_common(1)[0][1])
"""