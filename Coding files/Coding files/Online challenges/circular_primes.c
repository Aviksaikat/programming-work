#include<stdio.h>
#include<math.h>
int number_of_digits(int n) 
{ 
    int len = 0; 
    while (n > 0) 
	{ 
        len++; 
        n /= 10; 
    } 
    return len; 
} 

void circular_prime(long int num) 
{ 
    long int digits = number_of_digits(num); 
    long int powTen = pow(10, digits - 1);
	int i=0; 
  
    for (i = 0; i < digits - 1; i++) 
	{ 
  
        long int firstDigit = num / powTen; 
  
        // Formula to calculate left shift 
        // from previous number 
        long int left = ((num * 10) + firstDigit) - (firstDigit * powTen * 10); 
        // Update the original number 
        printf("%ld\n",left);
        num = left; 
    } 
} 
int main()
{
	long int i=0,n;
	int count=0,len=0;
	//while(i<1000000)
	{
		circular_prime(i);
		i++;
	}
	return 0;
}
