def main():
    t = int(input("Enter no. of test cases "))
    n = int(input())
    while(t>0):
        i=0
        j=0
        l = []
        count=0
        Gr = list(map(int,input().split()))
        As = list(map(int,input().split()))

        if (len(Gr) or len(As))>n:
            print("You have entered more values")
            exit(1)

        #print(Gr)
        for i in range(n):
            count = 0 
            for j in range(i):
                p = int(Gr[i]-As[j])
                if(p>0):
                    count+=1
                l.append(count)
        print("L = ",l)
        print(max(l))
        t-=1

main()
