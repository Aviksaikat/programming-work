def main():
    t = int(input())
    n = int(input())
    while(t>0):
        i=0
        j=0
        count=0
        Gr = list(map(int,input().split()))
        As = list(map(int,input().split()))

        if (len(Gr) or len(As))>n:
            print("You have entered more values")
            exit(1)
        Gr.sort(reverse=True)
        As.sort(reverse=True)
        #print(Gr)
        p=0
        for i in range(n): 
            for j in range(p,n):
                if(Gr[i]>As[j]):
                    p = j+1
                    count+=1
                    break
        print(count)
        t-=1

main()
