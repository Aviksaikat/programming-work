from collections import Counter
def most_common(lst):
    return( [Counter(col).most_common(1)[0][0] for col in zip(*lst)] )
l = [[1,2,3],[3,4,5],[7,8,9]]
print(most_common(l))