public class Example {
    public void foo() {
        System.out.println("foo() method with no parameters");
    }

    public void foo(int i) {
        System.out.println("foo() method with an integer parameter: " + i);
    }

    public void foo(String s) {
        System.out.println("foo() method with a string parameter: " + s);
    }

    public static void main(String[] args) {
        Example example = new Example();

        // Call the foo() method with no parameters
        example.foo();

        // Call the foo() method with an integer parameter
        example.foo(42);

        // Call the foo() method with a string parameter
        example.foo("Hello, world!");
    }
}
