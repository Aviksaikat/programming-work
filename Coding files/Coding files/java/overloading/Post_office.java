class overriding
{
    void interest(double p, int t)
    {
        double r = 1.8;
        double i = (r*p*t)/100;
        System.out.println("Interest form overriding class is: "+ i );
    }
}
public class Post_office extends overriding
{
    void interest(double p, int t)
    {
        //super.interest(p,t);
        double r = 2.1;
        double i =(r*p*t);
        System.out.println("Post Office interest: "+ i);
    }
    public static void main()
    {
        Post_office po = new Post_office();
        po.interest(20000,2);
    }
}
