#include<stdio.h>
#include<stdlib.h>
void bubbleSort(int *a,int n)//best O(n) & worst O(n^2)	
{	
	int i,j,count=0;
	int temp;
	for(i=0;i<n-1;i++)
	{
		int flag=0;//optimizing the best case run only once
		for(j=0;j<n-i-1;j++)
		{
			if(a[j]>a[j+1])
			{
				temp=a[j];
				a[j]=a[j+1];
				a[j+1]=temp;
				flag=1;
			}
		}
		count++;
		if(flag==0)
			break;
	}
	printf("Count = %d\n",count);
}
int main()
{
	int *p;
	int n;
	register int i;
	printf("Enter the length of the array : ");
	scanf("%d",&n);
	p=(int*)malloc(n*sizeof(int));
	printf("Enter elements for the array \n");
	for(i=0;i<n;i++)
		scanf("%d",&p[i]);
	bubbleSort(p,n);
	printf("Sorted array : ");
	for(i=0;i<n;i++)
		printf("%d ",p[i]);
	return 0;
}
