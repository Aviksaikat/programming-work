#include<stdio.h>
#include<conio.h>//turbo c
#include<stdlib.h>
int main()
{
  int m,n,o,p,i,j,k=0,cal=0;
  int **mat1,**mat2,**mat3;
  /*pointer to pointer means to store 1st address of the 2d array && and 2nd *      means to store the address of the first pointer*/
  clrscr();//only for turbo c
  printf("Enter the no. of rows and columns of the first matrix:\n");
  scanf("%d%d",&m,&n);
  printf("Enter the no. of rows and columns of the second matrix:\n");
  scanf("%d%d",&o,&p);
  if(n!=o)
  {
    printf("Error");
    exit(0);
  }
  mat1=(int**)calloc(m,sizeof(int*));
  for(i=0;i<m;i++)
  {
    mat1[i]=(int*)calloc(n,sizeof(int));
  }
  printf("Enter elements of matrix 1:\n");
  for(i=0;i<m;i++)
  {
    for(j=0;j<n;j++)
    {
      printf("Element[%d][%d]:",i+1,j+1);
      scanf("%d",&mat1[i][j]);
    }
  }
  mat2=(int**)calloc(o,sizeof(int*));
  for(i=0;i<o;i++)
  {
    mat2[i]=(int*)calloc(p,sizeof(int));
  }
  printf("Enter the elements of matrix 2:\n");
  for(i=0;i<o;i++)
  {
    for(j=0;j<p;j++)
    {
      printf("Element[%d][%d]:",i+1,j+1);
      scanf("%d",&mat2[i][j]);
    }
  }
  printf("The matrix 1 is:\n");
  for(i=0;i<m;i++)
  {
    for(j=0;j<n;j++)
    {
      printf("%3d",mat1[i][j]);//3 is for spacing to look elegent.
    }
    printf("\n");
  }
  printf("The matrix 2 is:\n");
  for(i=0;i<o;i++)
  {
    for(j=0;j<p;j++)
    {
      printf("%3d",mat2[i][j]);
    }
    printf("\n");
  }
  mat3=(int**)calloc(m,sizeof(int*));
  for(i=0;i<m;i++)
  {
    mat3[i]=(int*)calloc(p,sizeof(int));
  }
  for(i=0;i<m;i++)
  {
    for(j=0;j<p;j++)
     {
       for(k=0;k<o;k++)
       {
     cal=cal+mat1[i][k]*mat2[k][j];
       }
       mat3[i][j]=cal;
       cal=0;
     }
  }
  for(i=0;i<m;i++)
  {
    for(j=0;j<p;j++)
     printf("%3d",mat3[i][j]);
    printf("\n");
  }
  getch();//turbo c
  return 0;
}
