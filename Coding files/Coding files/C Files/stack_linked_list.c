#include<stdio.h>
#include<stdlib.h>
//LIFO @ begining can be done in at the end as well
struct node
{
	int data;
	struct node* link;
};
struct node* top=NULL;
void insert()
{
	struct node* p;
	p=(struct node*)malloc(sizeof(struct node));
	printf("Enter value: ");
	scanf("%d",&p->data);
	if(!p)
		printf("Stack Overflow\n");
	else
	{
		p->link=top;
		top=p;	
	}	
}
void deletee()
{
	if(top==NULL)
		printf("Stack Underflow\n");
	else
	{
		struct node* temp=top;
		printf("Deleted item -> %d",temp->data);
		top=temp->link;
		free(temp);
	}
}
void display()
{
	if(top==NULL)
		printf("Empty linked list\n");
	else
	{
		struct node* temp=top;
		printf("Elements in the linked list are : \n");
		while(temp!=NULL)
		{
			printf("%d ",temp->data);
			temp=temp->link;
		}
	}
}
int main()
{
	int j;
	do
	{
		printf("\n1.to insert\n2.to delete\n3. to display\n0. to exit\n");
		scanf("%d",&j);
		switch(j)
		{
			case 1:insert();
			   		break;
			case 2:deletee();
			   		break;
			case 3:display();
			   		break;
			case 0:printf("Bye");
			   		exit(0);
			default:printf("Opps wrong input\n");
		}
	}while(j!=0);
	return 0;
}
