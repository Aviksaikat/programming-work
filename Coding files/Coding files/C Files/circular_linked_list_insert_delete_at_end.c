#include<stdio.h>//insert & delete at end
#include<stdlib.h>
struct node
{
	int data;
	struct node* link;
};
struct node* head=NULL;
void insert()
{
	struct node* p;
	p=(struct node*)malloc(sizeof(struct node));
	printf("Enter data : ");
	scanf("%d",&p->data);
	if(head==NULL)
	{
		head=p;
		p->link=head;
	}
	else
	{
		struct node* temp=head;
		while(temp->link!=head)
			temp=temp->link;
		temp->link=p;
		p->link=head;	
	}
}
void deletee()
{
	if(head==NULL)
		printf("No elements\n");
	else
	{
		struct node* temp=head;
		if(temp->link==head)
		{
			printf("Deleted item -> %d",temp->data);
			head=NULL;
			free(temp);
		}
		else
		{
			while(temp->link->link!=head)
				temp=temp->link;
			printf("Deleted itme -> %d",temp->link->data);
			free(temp->link);
			temp->link=head;			
		}
	}
}
void display()
{
	if(head==NULL)
		printf("Empty linked list\n");
	else
	{
		struct node* temp=head;
		printf("Elements in the linked list are : \n");
		while(temp->link!=head)
		{
			printf("%d ",temp->data);
			temp=temp->link;
		}
		printf("%d",temp->data);
	}
}
int main()
{
	int j;
	do
	{
		printf("\n1.to insert\n2.to delete\n3. to display\n0. to exit\n");
		scanf("%d",&j);
		switch(j)
		{
			case 1:insert();
			   		break;
			case 2:deletee();
			   		break;
			case 3:display();
			   		break;
			case 0:printf("Bye");
			   		exit(0);
			default:printf("Opps wrong input\n");
		}
	}while(j!=0);
	return 0;
}
