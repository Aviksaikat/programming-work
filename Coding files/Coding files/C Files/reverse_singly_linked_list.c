#include<stdio.h>
#include<stdlib.h>
struct node
{
	int data;
	struct node* link;
};
struct node* head=NULL;
void insert()
{
	struct node* p;
	p=(struct node*)malloc(sizeof(struct node));
	printf("Enter value: ");
	scanf("%d",&p->data);
	p->link=NULL;
	if(head==NULL)
		head=p;
	else
	{
		struct node* temp=head;
		while(temp->link!=NULL)
			temp=temp->link;
		temp->link=p;
	}
		
}
void deletee()
{
	
	if(head==NULL)
		printf("Empty linked list\n");
	else
	{
		struct node* temp1=head;
		struct node* temp2;
		if(head->link==NULL)
			head=NULL;
		else
		{
			while(temp1->link!=NULL)
			{
				temp2=temp1;
				temp1=temp1->link;			
				
			}
			temp2->link=NULL;
		}
		printf("Deleted item -> %d",temp1->data);
		free(temp1);
	}
}
void rev()
{
	if(head==NULL)
		printf("No elements\n");
	else
	{
		struct node* temp=head;
		struct node* temp1=head;
		struct node* temp2=head;
		register int i;
		int n=0;
		int j,e,k;
		while(temp!=NULL)
		{
			temp=temp->link;
			n++;
		}
		j=n;//length
		for(i=0;i<n/2;i++)
		{
			if(i>0)
				temp1=temp1->link;
			temp2=head;
			j--;
			for(k=0;k<j;k++)
				temp2=temp2->link;
			e=temp1->data;
			temp1->data=temp2->data;
			temp2->data=e;
		}
	}
}
	
void display()
{
	if(head==NULL)
		printf("Empty linked list\n");
	else
	{
		struct node* temp=head;
		printf("Elements in the linked list are : \n");
		while(temp!=NULL)
		{
			printf("%d ",temp->data);
			temp=temp->link;
		}
	}
}
int main()
{
	int j;
	do
	{
		printf("\n1.to insert\n2.to delete\n3. to display\n4. to reverse\n0. to exit\n");
		scanf("%d",&j);
		switch(j)
		{
			case 1:insert();
			   		break;
			case 2:deletee();
			   		break;
			case 3:display();
			   		break;
			case 4:rev();
					break;
			case 0:printf("Bye");
			   		exit(0);
			default:printf("Opps wrong input\n");
		}
	}while(j!=0);
	return 0;
}
