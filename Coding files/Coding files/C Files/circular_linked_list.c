#include<stdio.h>//insert & delete at begining
#include<stdlib.h>
struct node
{
	int data;
	struct node* link;
};
struct node* head=NULL;
void insert()
{
	struct node* p;
	p=(struct node*)malloc(sizeof(struct node));
	printf("Enter element : ");
	scanf("%d",&p->data);
	//p->link=NULL;
	if(head==NULL)
	{
		head=p;
		p->link=head;
	}
	else
	{
		struct node* temp=head;
		while(temp->link!=head)
			temp=temp->link;
		p->link=head;
		head=p;
		temp->link=head;
	}
}
void deletee()
{
	if(head==NULL)
		printf("No elements\n");
	else
	{
		struct node* temp=head;
		struct node* last=NULL;
		if(temp->link==head)
		{
			printf("Deleted item -> %d",head->data);
			head=NULL;
			free(temp);
		}
		else
		{
		 	while(temp->link!=head)
		 		temp=temp->link;
		 	last = temp;
		 	temp = head;
		 	head = head->link;
			printf("Deleted item -> %d",temp->data);
			last->link=head;
			free(temp);
		}
	}

}	
void display()
{
	if(head==NULL)
		printf("Empty linked list\n");
	else
	{
		struct node* temp=head;
		printf("Elements in the linked list are : \n");
		while(temp->link!=head)
		{
			printf("%d ",temp->data);
			temp=temp->link;
		}
		printf("%d",temp->data);
	}
}
int main()
{
	int j;
	do
	{
		printf("\n1.to insert\n2.to delete\n3. to display\n0. to exit\n");
		scanf("%d",&j);
		switch(j)
		{
			case 1:insert();
			   		break;
			case 2:deletee();
			   		break;
			case 3:display();
			   		break;
			case 0:printf("Bye");
			   		exit(0);
			default:printf("Opps wrong input\n");
		}
	}while(j!=0);
	return 0;
}
