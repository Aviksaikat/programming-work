#include<stdio.h>
#include<conio.h>//n*log(n) in both case
#include<time.h>
void merge(int *a,int lb,int mid,int ub)
{
	int i=lb,j=mid+1,k=lb,aq,bq;
	int b[100];
	for(aq=0;aq<1000;aq++)
	for(bq=0;bq<10000;bq++);
	while(i<=mid && j<=ub)
	{
		if(a[i]<=a[j])
		{
			b[k]=a[i++]; 
		}
		else
		{
			b[k]=a[j++]; 
		}
		k+=1;
	}
	if(i>mid)
	{
		while(j<=ub)
		{
			b[k++]=a[j++];
		}
	}
	else
	{
		while(i<=mid)
		{
			b[k++]=a[i++];
		}
	}
	for(i = lb; i <= ub; i++)
      a[i] = b[i];
}
void mergeSort(int *a,int lb,int ub)
{
	if(lb<ub)
	{
		int mid=(lb+ub)/2;
		mergeSort(a,lb,mid);
		mergeSort(a,mid+1,ub);//these 2 lines dividing the array into parts while only e element left
		merge(a,lb,mid,ub);	
	}
}
int main()
{
	int *p;
	int n;
	register int i;
	int end,start;
	double result;	
	printf("Enter the length of the array " );
	scanf("%d",&n);
	p=(int*)malloc(n*sizeof(int));
	printf("Enter elements for the array \n");
	for(i=0;i<n;i++)
		scanf("%d",&p[i]);
	start = clock();
	printf("start: %d\n",start);
	mergeSort(p,0,n-1);
	end = clock();
	printf("end  : %d\n",end);
	printf("Sorted array : ");
	for(i=0;i<n;i++)
		printf("%d ",p[i]);
	result = (float)(end-start)/CLOCKS_PER_SEC;
	printf("\nTime taken -> %f",result);
	return 0;
}
