#include<stdio.h>
#include<time.h>
//for turboc
//#include<conio.h>
#define max 20 
int stack[max],visited[max],a[max][max];
int top=-1;
void push(int e)
{
	stack[++top] = e;
}

int pop()
{
	int e; 
	e = stack[top--];
	printf("%d\t",e);
	return e;
}

int stackEmpty()
{
	if(top==-1)
    	return 1;
  	else
    	return 0;
}

void cal(int sv,int n)
{
  	push(sv);
  	visited[sv] = 1;
  
  	while(!stackEmpty())
	{
		int i;
    	int v = pop();
    	for(i = n-1;i>=0;i--)
    	{
      		if(!(!(a[v][i])) && !(visited[i]))
      		{
				push(i);
				visited[i] = 1;
      		}
    	}
  	}
}

int main()
{
	printf("\nName: Saikat Karmakar\nRollno: 18700218016\nDepartment: Information Technology\nSemester: 4th \n");
	clock_t start,end;
  	double result;
	int i,j,sv,n;
  	//clrscr();
  	printf("\nEnter number of vertices : ");
  	scanf("%d",&n);
  	printf("\nEnter adjacency matrix : \n");
  	for(i=0;i<n;i++)
  	{
    	for(j=0;j<n;j++)
    	{
      		scanf("%d",&a[i][j]);
    	}
  	}
  	printf("\nEnter starting vertex : \n");
  	scanf("%d",&sv);
  	printf("Entered Adjacency matrix is\n");
	for(i=0;i<n;i++)
	{	
		for(j=0;j<n;j++)
		{
			printf(" %d",a[i][j]);
		}
		printf("\n");
	}
  	start=clock();
  	cal(sv,n);
  	end=clock();
  	result=(double)(end-start)/CLOCKS_PER_SEC;
  	printf("\nExecution Time -> %f",result);
  	//getch();
  	return 0;
}
