#include <stdio.h>
int main()
{
  float dist,in_m, in_cm, in_feet, in_inch;
  printf("Enter the distance: ");
  scanf("%f", &dist);
  
  in_m = dist * 1000;
  in_cm = dist * 100000;
  in_feet = dist * 3280.84;
  in_inch = dist * 39370.08;
  
  printf("%f\n%f\n%f\n%f\n" , in_m , in_cm , in_inch , in_feet);
}
