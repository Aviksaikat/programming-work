#include<stdio.h>
#include<stdlib.h>//best O(nlog(n)) && worst O(n^2)
int partition(int *a,int lb,int ub)
{
	register int start=lb,end=ub;
	int key=a[lb],temp;
	while(start<end)
    {
		while(a[start]<=key)
			start++;
		while(a[end]>key)
			end--;
		if(start<end)
		{
			temp=a[start];
			a[start]=a[end];
			a[end]=temp;
		}
	}
	temp=a[lb];
	a[lb]=a[end];
	a[end]=temp;
	return end;
}
void quickSort(int *a,int lb,int ub)
{
	if(lb<ub)
	{
		int loc=partition(a,lb,ub);
		quickSort(a,lb,loc-1);
		quickSort(a,loc+1,ub);
		//printf("YO\n");
	}
}
int main()
{
	int *p;
	int n;
	register int i;
	printf("Enter the length of the array : ");
	scanf("%d",&n);
	p=(int*)malloc(n*sizeof(int));
	printf("Enter elements for the array \n");
	for(i=0;i<n;i++)
		scanf("%d",&p[i]);
	quickSort(p,0,n-1);
	printf("Sorted array : ");
	for(i=0;i<n;i++)
		printf("%d ",p[i]);
	return 0;
}
