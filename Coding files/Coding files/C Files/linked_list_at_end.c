//insert and delete at end
#include<stdio.h>
#include<stdlib.h>
struct node
{
	int data;
	struct node* link;
};
struct node* head=NULL;
void insert()
{
	struct node* p;
	p=(struct node*)malloc(sizeof(struct node));
	printf("Enter value: ");
	scanf("%d",&p->data);
	p->link=NULL;
	if(head==NULL)
		head=p;
	else
	{
		struct node* temp=head;
		while(temp->link!=NULL)
			temp=temp->link;
		temp->link=p;
	}
		
}
void deletee()
{
	
	if(head==NULL)
		printf("Empty linked list\n");
	else
	{
		struct node* temp1=head;
		struct node* temp2;
		if(head->link==NULL)
			head=NULL;
		else
		{
			while(temp1->link!=NULL)
			{
				temp2=temp1;
				temp1=temp1->link;			
				
			}
			temp2->link=NULL;
		}
		printf("Deleted item -> %d",temp1->data);
		free(temp1);
	}
}
	
void display()
{
	if(head==NULL)
		printf("Empty linked list\n");
	else
	{
		struct node* temp=head;
		printf("Elements in the linked list are : \n");
		while(temp!=NULL)
		{
			printf("%d ",temp->data);
			temp=temp->link;
		}
	}
}
int main()
{
	int j;
	do
	{
		printf("\n1.to insert\n2.to delete\n3. to display\n0. to exit\n");
		scanf("%d",&j);
		switch(j)
		{
			case 1:insert();
			   		break;
			case 2:deletee();
			   		break;
			case 3:display();
			   		break;
			case 0:printf("Bye");
			   		exit(0);
			default:printf("Opps wrong input\n");
		}
	}while(j!=0);
	return 0;
}
