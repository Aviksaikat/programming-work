#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#define max 6
#define tab '\t'
#define space ' '
int top=-1;
int stack[100];
char infix[100];
char postfix[100];
//sym=symbol
int isempty()
{
	if(top==-1)
		return 1;
	else
		return 0;
} 
int pop()
{
	if(top==-1)
	{
		printf("Underflow\n");
	}
	else
	//{
		//printf(" Deleted item is : %d\n",stack[top]);
		//printf("top in pop = %d\n",top);
		//top-=1;
	//}
	//printf("##%d",top);
	    return stack[top--];
}
void push(long int sym)
{
	
	if(top==max-1)
	{
		printf("Overflow\n");
	}
	else
	{
		top+=1;
		stack[top]=sym;
		//printf(" top in push  = %d\n",top);	
	}
	//printf("%s\n",stack);
	
}
int prec(char sym) 
{ 
    switch (sym) 
    { 
    	case '+': 
    	case '-': 
        	return 1; 
  
    	case '*': 
    	case '/': 
        	return 2; 
  
    	case '^': 
        	return 3; 
    } 
    return -1; 
} 
int white_space(char sym)
{
	if(sym == space || sym == tab)
		return 1;
	else
		return 0;
}
void infix_to_postfix()
{
	unsigned int i=0,p=0;
	char next;
	char sym;
	for(i=0;i<strlen(infix);i++)
	{
		sym=infix[i];
		if(!white_space(sym))
		{
			switch(sym)
			{
				case'(':push(sym);
						break;
				case')':while((next=pop())!='(')
							postfix[p++]=next;
						break;
				case '+':
				case '-':
				case '*':
				case '/':
				case '^':
				case '%':
						while(!isempty() && prec(stack[top])>=prec(sym))
							postfix[p++]=pop();
						push(sym);
						break;
				default:
						postfix[p++]=sym;					
			}
		}
	}
	while(!isempty())
		postfix[p++]=pop();
	postfix[p]='\0';
}
int main()
{
	printf("Enter infix expression :\n");
	gets(infix);
	infix_to_postfix();
	//puts(infix);
	printf("Postfix expression is: %s",postfix);
	return 0;	
}
