#include <stdio.h>
#include <string.h>
#include <ctype.h>

int main(int argc, char** argv)
{
    char* word = argv[1];
    int key = 13;
	register int i=0;
    // all the letters in the first argument 
    if(islower(word))
    	printf("Opps you're using mixed ");
    for (i=0; i < strlen(word); i++)
    {
        int currentLetter = word[i];

        char cipher = currentLetter + key;

        // make sure the next letter isn't over 26 or it isn't a ascii letter
        // if it is, do %26
        if ((currentLetter - 'a') + key > 26)
        {
            key = ((currentLetter - 'a') + key) % 26;
            cipher = 'a' + key;
        }

        printf("%c", cipher);
        // reset the key and do the next letter
        key = 13;
    }
    return 0;
}
