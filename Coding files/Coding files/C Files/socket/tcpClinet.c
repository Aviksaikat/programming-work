//this is a tcp client which will connect to the tcp server
#include<stdio.h>
#include <stdlib.h>

#include<sys/types.h>
//#include<sys/socket.h> //scoket in not for win
#ifdef __WIN32__
# include<Winsock2.h>
#else
# include<sys/socket.h>
//store address info.
#include<netinet/in.h>
#endif

int main(argc,argv)
{
    //create a socket/setup
    int network_socket;
    //AF_INET = ipv4, SOCK_STREAM = tcp,0 = protocol(deafult)
    network_socket = socket(AF_INET,SOCK_STREAM,0);

    //specifing a connecting address
    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    //specify the port
    server_address.sin_port = htons(9002); ;
    //addr(ip), INADDR_ANY= any ip of localHost
    server_address.sin_addr.s_addr = INADDR_ANY;

    int connection_status = connect(network_socket,(struct sockaddr *) &server_address,sizeof(server_address));
    //check for errors
    if(!connection_status)
    {
        printf("There is an error connecting to the remote socket\n");       
    }
    
    //receive data from the server
    char server_response[256];
    recv(network_socket,&server_response,sizeof(server_response),0);
    
    //print out server's response
    printf("The server sent the data: %s\n",server_response);
    
    //close the connection
    close(network_socket);
    return 0;
}
