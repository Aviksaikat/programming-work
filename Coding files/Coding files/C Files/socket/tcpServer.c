//this is a tcp server which will connect to the tcp client
#include<stdio.h>
#include<stdlib.h>

#include<sys/types.h>
#ifdef __WIN32__
#include<Winsock2.h>
#else
#include<sys/socket.h>
//store address info.
#include<netinet/in.h>
#endif


int main(argc,argv)
{
    //hold the data sent to the client
    char server_message[256] = "You have connected to the server";
    
    //create a socket/setup
    int server_socket;
    server_socket = socket(AF_INET,SOCK_STREAM,0);

    //specifing a server address
    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    //specify the port
    server_address.sin_port = htons(9002); ;
    //addr(ip), INADDR_ANY= any ip of localHost
    server_address.sin_addr.s_addr = INADDR_ANY;
    
    //bind the socket to the specific ip
    bind(server_socket,(struct sockaddr*) &server_address,sizeof(server_address));
    // 5 = no. of connections
    listen(server_socket,5);

    int clinet_socket;
    clinet_socket = accept(server_socket,NULL,NULL);

    //send the message
    send(clinet_socket,server_message,sizeof(server_message),0);
    close(server_socket);
    return 0;

}