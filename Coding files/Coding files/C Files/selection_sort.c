#include<stdio.h>
#include<stdlib.h>
#include<time.h>
void selectionSort(int *a,int n)//O(n^2) complexity for best & worst
{
	register int i,j;
	int temp;
	for(i=0;i<n-1;i++)
	{
		int min=i;
		for(j=i+1;j<n;j++)
		{
			if(a[j]<a[min])
				min=j;
		}
		if(i!=min)
		{
			temp=a[i];
			a[i]=a[min];
			a[min]=temp;
		}
	}
}
int main()
{
	int n,*p;
	register int i;
	printf("Enter the length of the array : ");
	scanf("%d",&n);
	p=(int*)malloc(n*sizeof(int));
	printf("Enter elements for the array \n");
	for(i=0;i<n;i++)
		scanf("%d",&p[i]);
	selectionSort(p,n);
	printf("Sorted array : ");
	for(i=0;i<n;i++)
		printf("%d ",p[i]);
	/*clock_t t; 
    t = clock();  
    t = clock() - t; 
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds 
    printf("took %f seconds to execute \n", time_taken); */
	return 0;
}
