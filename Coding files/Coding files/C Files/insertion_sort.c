#include<stdio.h>
#include<stdlib.h>
void insertionSort(int *a,int n)// best case O(n) && worst case O(n^2)
{
	register int i,j;
	for(i=1;i<n;i++)
	{
		int temp=a[i];
		j=i-1;
		while(j>=0 && a[j]>temp)
		{
			a[j+1]=a[j];
			j--;	
		}
		a[j+1]=temp;
	}
}
int main()
{
	int *p;
	int n;
	register int i;
	printf("Enter the length of the array : ");
	scanf("%d",&n);
	p=(int*)malloc(n*sizeof(int));
	printf("Enter elements for the array \n");
	for(i=0;i<n;i++)
		scanf("%d",&p[i]);
	insertionSort(p,n);
	printf("Sorted array : ");
	for(i=0;i<n;i++)
		printf("%d ",p[i]);
	return 0;
}
