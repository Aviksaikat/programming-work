/*
Create a function with two arguments that will return an array of the first n multiples of x.

Assume both the given number and the number of times to count will be positive numbers greater than 0.

Return the results as an array or list ( depending on language ).

Examples
count_by(1, 10) // should return vec![1,2,3,4,5,6,7,8,9,10]
count_by(2,5) // should return vec![2,4,6,8,10]
*/

fn count_by(x: u32, n: u32) -> Vec<u32> {
    let mut vec: Vec<u32> = Vec::new();

    for i in 1..n + 1 {
        vec.push(x * i);
    }
    return vec;
}

//? Community

fn count_by(x: u32, n: u32) -> Vec<u32> {
    (1..=n).map(|v| v * x).collect()
}

pub fn main() {
    assert_eq!(count_by(1, 10), vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
    assert_eq!(count_by(2, 5), vec![2, 4, 6, 8, 10]);
    assert_eq!(count_by(3, 7), vec![3, 6, 9, 12, 15, 18, 21]);
    assert_eq!(count_by(50, 5), vec![50, 100, 150, 200, 250]);
    assert_eq!(count_by(100, 6), vec![100, 200, 300, 400, 500, 600]);
}
