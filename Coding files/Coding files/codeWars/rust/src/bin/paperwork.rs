fn paperwork(n: i16, m: i16) -> u32 {
    if n < 0 || m < 0 {
        return 0;
    } else {
        return n as u32 * m as u32;
    }
}

pub fn main() {
    assert_eq!(paperwork(5, 5), 25);
    assert_eq!(paperwork(5, -5), 0);
    assert_eq!(paperwork(-5, -5), 0);
    assert_eq!(paperwork(5, 0), 0);
}
