fn repeat_str(src: &str, count: usize) -> String {
    let mut string: String = String::new();
    for _i in 0..count {
        string += src;
    }
    return string;
}

//*  community Sols
fn repeat_str(src: &str, count: usize) -> String {
    src.repeat(count)
}

pub fn main() {
    assert_eq!(repeat_str("a", 4), "aaaa");
    assert_eq!(repeat_str("hello ", 3), "hello hello hello ");
    assert_eq!(repeat_str("abc", 2), "abcabc");
}
