use std::convert::TryInto;
fn digitize(n: u64) -> Vec<u8> {
    if n < 10 {
        return vec![n.try_into().unwrap()];
    }
    let mut digits = Vec::new();
    let mut num = n;
    while num > 0 {
        digits.push((num % 10).try_into().unwrap());
        num /= 10;
    }
    return digits;
}


pub fn main() {
    assert_eq!(digitize(348597), vec![7,9,5,8,4,3]);
    assert_eq!(digitize(35231), vec![1,3,2,5,3]);
    assert_eq!(digitize(0), vec![0]);
}

// Phind Solution
/*

fn reverse_digits(num: u32) -> Vec<u32> {
   let num_string = num.to_string();
   let reversed_digits: Vec<u32> = num_string.chars().rev().map(|c| c.to_digit(10).unwrap()).collect();
   reversed_digits
}
*/