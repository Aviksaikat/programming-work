# Run
```
go build file.go
./file.exe  
or
go run file.go
``` 

# Imports

```go
import "fmt"

import (
"fmt")
```

# Declaring variables

## Static  
```go
var i int
i = 10

var i int = 10[**use it as package level declaration(i.e outside main)**]
```
## Dynamic
```go
i := 10
```
## Multiple var declaration
```go
var (
firstName string = "Tommy"
lastName  string = "Shelby"
company   string = "Peaky Blinders"
time  int= 1921
)
```
>upper case vars scoped outside to all packages

> main level vars gets more priority

> %v = value , %T = type

# Prints
```go
Println // new line at the end
Print // no new line 
Printf // formatted string i.e can use %v %T etc.
```
# Type conversion
```go
var i float32 = 41.5
var j int
j = int(i)
can't convert to sting directly bcz it compares bytes 
so use the package `strconv`
```
![image info](./pictures/var1.jpg)

------------------------------

![image info](./pictures/var2.jpg)

# Data Types
## can use complex numbers in Golang
```go
integrs = int8, int16, int32, int64
unsigned ints = unit8, ......(no 64bit)

floats = float32, float64
n := 3.14 // always be float64 
n = 13.7e22
n = 2.1E14
no remainder for float

complex = complex64, complex128

var c complex64 = 1 - 2i
var c complex128 = complex(1, -2)
c := 1 - 2i
---------------------------------------
fmt.Printf("%v %T\n", real(c), real(c))
fmt.Printf("%v %T\n", imag(c), imag(c))

                **Output**
                -2 float32
                 1 float32
*they are complex64 type so 32 + 32 = 64

**Rune**
Rune literals are just 32-bit integer values (however they are untyped constants, so their type can change). They represent unicode codepoints.
r := 'a' 
```  

# General Bit shifting 
```go
a = 8
a << 3 = 64 # 2^3 * 2^3 
i.e. a << n => (2^something = which is equal to a) * 2^n
a >> 3 = 1  # 2^3 / 2^3
```
# Strings 
```go
s := "this is a string"
b := []byte(s) // convert the str to bytes i.e. uint(unicode) i.e ascii
```
> we can change package level constant by overwriting it by inner constant

# Constant Block
```go
const cons = iota
const (
_ = i //ignore the 1st value by assigning it to blank identifier
c1 = iota
c2 = iota
c3 = iota
)
```

# Loops
```go
// Golang only has the for loop
for i := 0; i < 10; i++ {
    // i
}

// The first and third parameters are ommitable
// For as a while
i := 0;

for i < 10 {
    i++
}

// Forever loop
for {

}

```
# Array
```go
// Declaration with specified size
var array [3]string
array[0] = "Hello"
array[1] = "Golang"
array[2] = "World"

// Declaration and Initialization
values := [5]int{1, 2, 3, 4, 5}

// Slices: A subarray that acts as a reference of an array
// Determining min and max
values[1:3] // {2, 3, 4}

// Determining only max will use min = 0
values[:2] // {1, 2, 3}

// Determining only min will use max = last element
values[3:] // {3, 4}

// Length: number of elements that a slice contains
len(values) // 5

// Capacity: number of elements that a slice can contain
values = values[:1]
len(values) // 2
cap(values) // 5

// Slice literal
slice := []bool{true, true, false}

// make function: create a slice with length and capacity
slice := make([]int, 5, 6) // make(type, len, cap)

// Append new element to slice
slice := []int{ 1, 2 }
slice = append(slice, 3)
slice // { 1, 2, 3 }
slice = append(slice, 3, 2, 1)
slice // { 1, 2, 3, 3, 2, 1 }

// For range: iterate over a slice
slice := string["W", "o", "w"]

for i, value := range slice {
i // 0, then 1, then 2
value // "W", then "o", then "w"
}

// Skip index or value

for i := range slice {
i // 0, then 1, then 2
}

for _, value := range slice {
value // "W", then "o", then "w"
}
```

