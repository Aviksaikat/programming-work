package main

//import "fmt"
//same as
import (
	"fmt"
	"strconv"
)

//const cons = iota
const (
	c1 = iota
	c2 = iota
	c3 = iota
)

//can declare vars at package level can't use := here
var a int = 420

func main() {
	a := 69
	//var a int = 69
	fmt.Println("Hellow Go!")
	fmt.Println(a)
	a = 786
	fmt.Println(a)
	var (
		firstName string = "Tommy"
		lastName  string = "Shelby"
		company   string = "Peaky Blinders"
		time      int    = 1921
	)
	fmt.Println(firstName, lastName, company, time)

	var i float32 = 41.5
	var j int
	j = int(i)
	fmt.Printf("%v, %T\n", j, j)

	//convert int to string
	var str string
	str = strconv.Itoa(j)

	fmt.Printf("Converted to String: %v,\nType %T", str, str)

	f1 := 13.7e22
	f2 := 3.14

	fmt.Printf("%v, %T\n", f1, f1)
	fmt.Printf("%v, %T", f2, f2)

	//var c complex64 = 1 - 2i
	//c := 1 - 2i
	var c complex128 = complex(1, 2)

	fmt.Println("\n------------Complex------------")
	fmt.Printf("%v, %T", c, c)

	fmt.Println("\n------------Imaginary & Real------------")
	fmt.Printf("%v, %T\n", real(c), real(c))
	fmt.Printf("%v, %T\n", imag(c), imag(c))

	fmt.Println("------------String------------")
	s := "this is a string"
	b := []byte(s)

	fmt.Printf("String: %v, %T\n", s, s)
	fmt.Printf("ASCII: %v, %T\n", b, b)

	fmt.Println("------------Rune------------")
	var r1 rune = 'a'
	r2 := 'a'
	fmt.Printf("%v, %T\n", r1, r1)
	fmt.Printf("%v, %T\n", r2, r2)

	fmt.Println("------------Constants------------")
	fmt.Println("c1 =", c1)
	fmt.Println("c2 =", c2)
	fmt.Println("c3 =", c3)

	fmt.Println("------------Arrays------------")
	//values := [5]int{1, 2, 3, 4, 5}
	//values := [...]int{1, 2, 3, 4, 5}
	//var values []int{1, 2, 3, 4, 5}
	values := []int{1, 2, 3, 4, 5}
	fmt.Println(values)

}
