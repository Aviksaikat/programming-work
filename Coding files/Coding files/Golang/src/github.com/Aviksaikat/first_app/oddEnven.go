package main

import "fmt"

func main() {

	var n int
	fmt.Print("Enter a number: ")
	fmt.Scanln(&n)
	if n%2 == 0 {
		fmt.Printf("%v is Even\n", n)
	} else {
		fmt.Printf("%v is Odd\n", n)
	}
}
