#creating a tcp server
import socket

server_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)#AF_INET= ipv4 & SOCK_STREAM = protocol
#host is ip address of the host
host = socket.gethostname()
port = 444#can be anything

server_socket.bind((host, port))

server_socket.listen(3)#no of connections

while True:
    clientsocket, address = server_socket.accept()
    print("Received connecton from %r" % str(address))

    message = "Thank you for connecting to the server" + "\r\n"
    clientsocket.send(message.encode("UTF-8"))
    clientsocket.close()
