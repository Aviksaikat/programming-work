#!/usr/bin/python3 
#? Simple Port Scanner
#* importing
import socket
import sys 
from datetime import datetime as dt

STARTING_PORT = 1
ENDING_POINT = 80

if (len(sys.argv) == 2):
    #*translating hostname to IPv4
    # if someone puts hostname incase of ip the script should run so this line
    target = socket.gethostbyname(sys.argv[1])
else:
    print("Invalid amount of arguments")
    print("Syntax: python3 portScanner.py <ip>")
    sys.exit(100)

#?adding pretty banner
print('-'*50)
print(f"Scanning target: {target}")
print("Time started: "+str(dt.now()))
print('-'*50)

try:
    for port in range(STARTING_PORT,ENDING_POINT):
        #*socket.AF_INET = IPv4, socket.SOCK_STREAM = port
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #*wait 1 sec for response otherswise skip the port
        socket.setdefaulttimeout(1)
        #*connect_ex return -> port open ? 0 : 1 (open = 0 else 1) 
        result = s.connect_ex((target,port))
        if(result):
            print("Port {} is open".format(port))
        s.close()

except KeyboardInterrupt:
    print("\nExiting Program!!")
    print("Byee!!")
    sys.exit(1)

#*no hostname resolution
except socket.gaierror:
    print("Hostname could not be resolved")
    sys.exit(2)

#*can't connect to the hostname(ip)
except socket.error:
    print("Could not connect to the server")
    sys.exit(2)

        
