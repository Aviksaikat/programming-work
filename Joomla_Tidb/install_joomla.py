import paramiko

# Define the application nodes
app_nodes = [("app_node1", "user1", "password1"), ("app_node2", "user2", "password2")]

# Install Joomla CMS on each node
for app_node, username, password in app_nodes:
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(app_node, username=username, password=password)
    command = "install Joomla CMS"
    stdin, stdout, stderr = ssh.exec_command(command)
    result = stdout.read().decode("utf-8") + stderr.read().decode("utf-8")
    if "Joomla CMS installed successfully" in result:
        print(f"Joomla CMS installed successfully on {app_node}")
    else:
        print(f"Failed to install Joomla CMS on {app_node}: {result}")
    ssh.close()
