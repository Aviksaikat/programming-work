import configparser

# Create the configuration file
config = configparser.ConfigParser()

# Define the nodes in the cluster
nodes = ["node1", "node2", "node3", "node4"]

# Configure each node
for node in nodes:
    config[node] = {
        "pd-address": f"{node}:2379",
        "tikv-address": f"{node}:20161",
        "tidb-port": "4000",
        "tikv-port": "20160"
    }

# Write the configuration to a file
with open("tidb.cfg", "w") as file:
    config.write(file)
