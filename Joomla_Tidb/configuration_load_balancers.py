import paramiko

# Define the load balancers
load_balancers = [("load_balancer1", "user1", "password1"), ("load_balancer2", "user2", "password2")]

# Define the application nodes
app_nodes = ["app_node1", "app_node2"]

# Configure the load balancers
for load_balancer, username, password in load_balancers:
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(load_balancer, username=username, password=password)
    for app_node in app_nodes:
        command = f"configure load balancing for {app_node}"
        stdin, stdout, stderr = ssh.exec_command(command)
        result = stdout.read().decode("utf-8") + stderr.read().decode("utf-8")
        if "Load balancing configured successfully" in result:
            print(f"Load balancing configured successfully on {load_balancer} for {app_node}")
        else:
            print(f"Failed to configure load balancing on {load_balancer} for {app_node}: {result}")
    ssh.close()
