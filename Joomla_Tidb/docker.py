import docker

client = docker.from_env()

# Load balancer 1
container1 = client.containers.run("image_name", ports={'22/tcp': ('0.0.0.0', 2221)}, name="lb1", detach=True)

# Load balancer 2
container2 = client.containers.run("image_name", ports={'22/tcp': ('0.0.0.0', 2222)}, name="lb2", detach=True)

# Application node 1
container3 = client.containers.run("image_name", ports={'22/tcp': ('0.0.0.0', 2223)}, name="app1", detach=True)

# Application node 2
container4 = client.containers.run("image_name", ports={'22/tcp': ('0.0.0.0', 2224)}, name="app2", detach=True)
