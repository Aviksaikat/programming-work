# ssh_connection.py

import paramiko

def connect_ssh(node, username, password):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(node, username=username, password=password)
    return ssh

def close_ssh(ssh):
    ssh.close()
