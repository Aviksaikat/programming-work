import paramiko
import subprocess

# Define the nodes in the cluster
nodes = [("node1", "user1", "password1"), ("node2", "user2", "password2"), ("node3", "user3", "password3"), ("node4", "user4", "password4")]

# Start tidb-server on each node
for node, username, password in nodes:
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(node, username=username, password=password)
    stdin, stdout, stderr = ssh.exec_command("tidb-server --config=tidb.cfg")
    result = stdout.read().decode("utf-8") + stderr.read().decode("utf-8")
    if "Listening on 0.0.0.0:4000" in result:
        print(f"tidb-server started successfully on {node}")
    else:
        print(f"tidb-server failed to start on {node}: {result}")
    ssh.close()
