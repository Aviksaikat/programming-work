import subprocess
from ssh_connection import connect_ssh, close_ssh

# Define the nodes in the cluster
nodes = ["node1", "node2", "node3", "node4"]

# Connect to each node via SSH
for node in nodes:
    ssh = connect_ssh(node, username="username", password="password")
    
    # Install tidb-server
    stdin, stdout, stderr = ssh.exec_command("yum update")
    stdin, stdout, stderr = ssh.exec_command("yum install -y tidb-server")
    
    # Install tidb-client
    stdin, stdout, stderr = ssh.exec_command("yum install -y tidb-client")
    
    # Install pd
    stdin, stdout, stderr = ssh.exec_command("yum install -y pd")
    
    # Install tikv
    stdin, stdout, stderr = ssh.exec_command("yum install -y tikv")
    
    close_ssh(ssh)

# Set up load balancing using HAProxy
result = subprocess.run(["yum", "install", "-y", "haproxy"])

# Configure HAProxy
with open("haproxy.cfg", "w") as file:
    file.write("frontend tidb-frontend\n")
    file.write("    bind *:4000\n")
    file.write("    default_backend tidb-backend\n")
    file.write("\n")
    file.write("backend tidb-backend\n")
    file.write("    balance roundrobin\n")
    file.write("    server node1 node1:4000 check\n")
    file.write("    server node2 node2:4000 check\n")

# Start HAProxy
result = subprocess.run(["haproxy", "-f", "haproxy.cfg"])
