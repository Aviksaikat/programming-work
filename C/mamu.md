# Normal Order
- First we're calculating `2 * 1`, then `2 * 2` .... `2 * 10`.

```c
void mutTable(int num, int range) {
    if (range > 10)
        return;
    
    printf("%d x %d = %d\n", num, range, num * range);
    mutTable(num, range + 1);
}
```

- Suppose we need the mut table of 2. So we'll call the `mutTable()` by using the arguments `2` & `1`. 

```c
mutTable(2, 1);
```
- The output
```bash
2 x 1 = 2
2 x 2 = 4
2 x 3 = 6
2 x 4 = 8
2 x 5 = 10
2 x 6 = 12
2 x 7 = 14
2 x 8 = 16
2 x 9 = 18
2 x 10 = 20
```

- For the first iteration the values of `num` & `range` should be `2` & `1` respectively.

```c
    if (range > 10)
        return;
```
- The `if (range > 10)` breaks the recursion loop if the range value reaches more than 10. Ex. (2 * 10) = 20 break. Increase this if you want tables more that mut of 10.
- Next basic c priting `printf("%d x %d = %d\n", num, range, num * range);` to output the values.

- Lastly
```c
mutTable(num, range + 1);
```
- So we're calling the same function with a slight diff arguments. For the next step the values will be like this(for table of 2) 
```c
mutTable(2, 1);
mutTable(2, 2);
[...]
mutTable(2, 10);
```
- If the value reaches more than 10 break the recursion loop.



## In reverse order
- First we're calculating `2 * 10`, then `2 * 9` .... `2 * 1`.

```c
void mutTable_rev(int num, int range) {
    if (range == 0)
        return;
    
    mutTable(num, range - 1);
    printf("%d x %d = %d\n", num, range, num * range);
    
}
```


# Final code

```C
#include<stdio.h>

void mutTable(int num, int range) {
    if (range > 10)
        return;
    
    printf("%d x %d = %d\n", num, range, num * range);
    mutTable(num, range + 1);
}

void mutTable_rev(int num, int range) {
    if (range == 0)
        return;
    
    mutTable(num, range - 1);
    printf("%d x %d = %d\n", num, range, num * range);
    
}



int main() {
    // printf("Hellow\n");
    //* Multiplication Table using recursion
    int n = 0;
    
    printf("Enter a value: ");
    scanf("%d", &n);
    // printf("%d\n", n);

    mutTable(n, 1);

    // mutTable_rev(n, 10);

    return 0;

}
```