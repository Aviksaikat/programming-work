#include<stdio.h>

void mutTable(int num, int range) {
    if (range > 10)
        return;
    
    printf("%d x %d = %d\n", num, range, num * range);
    mutTable(num, range + 1);
}

void mutTable_rev(int num, int range) {
    if (range == 0)
        return;
    
    mutTable(num, range - 1);
    printf("%d x %d = %d\n", num, range, num * range);
    
}



int main() {
    // printf("Hellow\n");
    //* Multiplication Table using recursion
    int n = 0;
    
    printf("Enter a value: ");
    scanf("%d", &n);
    // printf("%d\n", n);

    mutTable(n, 1);

    // mutTable_rev(n, 10);

    return 0;

}