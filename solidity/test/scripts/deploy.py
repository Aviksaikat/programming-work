#!/usr/bin/python3
from brownie import Test
from scripts.helpful_scripts import get_account


def deploy():
    owner, _ = get_account()
    test = Test.deploy({"from": owner})
    return test, owner


def main():
    deploy()

if __name__ == '__main__':
    main()