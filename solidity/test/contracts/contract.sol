pragma solidity ^0.8.17;


contract Test {
    address public owner;

    constructor() {
        owner = msg.sender;
    }

    function _address() public view returns(address){
        return address(this);
    }
}