package main

import (
	"fmt"
	"math"
)

/*
You might know some pretty large perfect squares. But what about the NEXT one?

Complete the findNextSquare method that finds the next integral perfect square after the one
passed as a parameter. Recall that an integral perfect square is an integer n such that sqrt(n)
is also an integer.

If the parameter is itself not a perfect square then -1 should be returned. You may assume the
parameter is non-negative.

Examples:(Input --> Output)

121 --> 144
625 --> 676
114 --> -1 since 114 is not a perfect square
*/

func FindNextSquare(sq int64) int64 {
	var sqrtNum int64 = int64(math.Sqrt(float64(sq)))

	//println(sqrtNum)
	//println(int64(math.Pow(float64(sqrtNum), 2)))

	if sq != int64(math.Pow(float64(sqrtNum), 2)) {
		return -1
	}
	return int64(math.Pow(float64(sqrtNum+1), 2))
}

func main() {

	num := FindNextSquare(int64(121))
	fmt.Println(num)

	if num != int64(144) {
		panic("Oopps!")
	}
}
