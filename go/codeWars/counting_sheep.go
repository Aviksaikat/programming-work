package main

/*
Consider an array/list of sheep where some sheep may be missing from their place.
We need a function that counts the number of sheep present in the array (true means present).

Hint: Don't forget to check for bad values like null/undefined
*/

import "fmt"

func CountSheeps(numbers []bool) int {
	count := 0

	for _, sheep := range numbers {
		if sheep {
			count++
		}
	}
	return count
}

func main() {
	sheeps := []bool{
		true, true, true, false,
		true, true, true, true,
		true, false, true, false,
		true, false, false, true,
		true, true, true, true,
		false, false, true, true,
	}
	count := CountSheeps(sheeps)
	fmt.Println(count)

	if count != 17 {
		panic("Oopps value is not 17!")
	}
}
