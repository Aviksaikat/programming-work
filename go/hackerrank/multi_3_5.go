package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func sum(n int32, k int32) int32 {
	d := n / k
	return k * (d * (d + 1)) / 2
}

func solve(n int32) int32 {
	return sum(n, 3) + sum(n, 5) - sum(n, 15)
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 16*1024*1024)

	tTemp, err := strconv.ParseInt(strings.TrimSpace(readLine(reader)), 10, 64)
	checkError(err)
	t := int32(tTemp)

	for tItr := 0; tItr < int(t); tItr++ {
		nTemp, err := strconv.ParseInt(strings.TrimSpace(readLine(reader)), 10, 64)
		checkError(err)
		n := int32(nTemp)

		//fmt.Println(n)
		//var i, sum int32
		//for i = 0; i < n; i++ {
		//	//if i%5 == 0 || i%3 == 0 {
		//	//	sum += i
		//	//}
		//	solve()
		//}
		fmt.Println(solve(n - 1))
	}
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
