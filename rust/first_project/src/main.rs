fn main() {
    println!("Hello, world!");

    //? implicit type declaration
    let x = 69;
    //? explicit type declaration
    let y: u32 = 420;
    println!("x : {}, y : {}", x, y);

    // let mut x = 100;
    // println!("Mutated x : {}", x);

    //* Making a new scope
    {
        //? y of the outer scope can be used inside of this scope
        // let y = y -1;
        let y = 14;
        println!("Inside scope y : {}", y);
    }

    //? Things can be done
    // let y: u32 = 400;
    let y: u32 = y + 100;
    println!("New y : {}", y);

    let y = "hahhaha";
    println!("New String y : {}", y);

    //? Constants
    const SECONDS: u32 = 60;
    println!("{}", SECONDS);

    
}
