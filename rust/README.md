# Rust Notes

> Saikat Karmakar | July 2022

---

- Compiling & running
```
rustc main.rs
cargo build
```
- Creating a package
```
cargo new <package_name>
```
- Directly compiling & running
```
cargo run
```
- Check code without compiling 
```
cargo check
```

- All variables are immutable in rust by default. We can explicitly change the value of variable using the keyword `mut`
- Scalar Types
    - signed integers: `i8, i16, i32, i64, i128 and isize (pointer size)`
    - unsigned integers: `u8, u16, u32, u64, u128 and usize (pointer size)`
    - floating point: `f32, f64`
    - char Unicode scalar values like `a`, `α` and `∞` (4 bytes each)
    - bool either `true` or `false`
    - and the `unit` type (), whose only possible value is an empty tuple: ()`

- to ignore a placeholder var use `_` at the start of the var name. 
- to remove all warnings use `#![allow(unused)]` at the beginning of a program. 

- ***Resource***
    - https://doc.rust-lang.org/