//! to remove all warnings
#![allow(unused)]

//* different crate(packages) import 
//? have to import rand package in the Cargo.toml file
use std::io;
//* import multiple packages
use std::io::{Write, BufReader, ErrorKind};
use std::fs::File;
use rand::Rng;
use std::cmp::Ordering;


fn input_output() {
    println!("What's your name? ");
    
    let mut name: String = String::new();
    let greeting: &str = "Nice to meet you";
    
    io::stdin().read_line(&mut name)
        //? error handling
        .expect("Didn't got your name!");
    //? trip_end to remove new lines & empty spaces from the input
    
    println!("Hello {}! {}", name.trim_end(), greeting);
}

fn data_types() {
    const ONE_MIL: u32 = 1_000_000; // same as 1000000
    const PI: f32 = 3.141592;
    //* variable shadowing
    //* i.e. same var name with diff. data types
    let age: &str = "68";
    //let mut age: u32 = 69;
    //? type casting
    let mut age: u32 = age.trim().parse()
        .expect("Age wasn't a number");
    age += 1;

    //? to ignore a placeholder var use '_' at the start of the var name
    let is_true: bool = true;
    let blood_group = 'C' ; 

    println!("I'm {} & I Want ${} & my blood group is {}", age, ONE_MIL, blood_group);
}

fn var_size() {
    println!("MAX size of i16 : {}", i16::MAX);
    println!("MAX size of u32 : {}", u32::MAX);
    println!("MAX size of u64 : {}", u64::MAX);
    println!("MAX size of usize : {}", usize::MAX);
    println!("MAX size of u128 : {}", u128::MAX);
    println!("MAX size of f32 : {}", f32::MAX);
    println!("MAX size of f64 : {}", f64::MAX);
}

fn random_num() {
    let random: i32 = rand::thread_rng().gen_range(1..101);
    println!("Random No. : {}", random);
}

fn conditional_statement() {
    let age = 8;

    if(age > 18) {
        println!("You're an adult");
    }
    else {
        println!("You're not an adult");
    }
    //* else are same as other langs
    
    //? ----- TERNARY OPERATOR -----
    let mut my_age = 47;
    let can_vote = if my_age >= 18 {
        true
    } else {
        false
    };
    println!("Can Vote : {}", can_vote);
}

fn match_statement() {
    //? ----- MATCH ----- i.e. switch case 
    // Match runs different code depending on conditions
    // The pattern and the code to be executed is called an arm
    // A match must match all possible values!

    // You can do what we did with if
    let age2 = 8;
    match age2 {
        1..=18 => println!("Important Birthday"), // 1 through 18
        21 | 50 => println!("Important Birthday"), // 21 or 50
        65..=i32::MAX => println!("Important Birthday"), // > 65
        _ => println!("Not an Important Birthday"), // Default like switch case
    };

    // Compares age to valid age and cmp returns an Ordering which
    // has either the value Less, Greater, or Equal
    let my_age = 18;
    let voting_age = 18;
    match my_age.cmp(&voting_age) {
        Ordering::Less => println!("Can't Vote"),
        Ordering::Greater => println!("Can Vote"),
        Ordering::Equal => println!("You just gained the right to vote!"),
    };
}


fn arrays_loops() {
    //? ----- ARRAYS -----
    // Elements in an array must be of the same type
    // and have a fixed size
    let arr_1 = [1,2,3,4];

    // Get value by index starting at 0
    println!("1st : {}", arr_1[0]);

    // Get array length
    println!("Length : {}", arr_1.len());

    //? ----- LOOP -----
    // Create an infinite loop that ends when break is called
    let arr_2 = [1,2,3,4,5,6,7,8,9];
    let mut loop_idx = 0;
    loop {
        if arr_2[loop_idx] % 2 == 0 {
            loop_idx += 1;
            continue; // Goes to beginning of loop
        }

        if arr_2[loop_idx] == 9 {
            break; // Breaks out of loop
        }

        println!("Val : {}", arr_2[loop_idx]);
        loop_idx += 1;
    }

    //? ----- WHILE LOOP -----
    // Looping based on a condition
    loop_idx = 0;
    while loop_idx < arr_2.len(){
        println!("Arr : {}", arr_2[loop_idx]);
        loop_idx += 1;
    }

    //? ----- FOR LOOP -----
    // For works better for cycling through collections
    for val in arr_2.iter() {
        println!("Val : {}", val);
    }
}


fn main() {
    
}
