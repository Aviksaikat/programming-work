//? import the file
// mod print;
// mod vars;
// mod types;
//mod stringz;
mod tuples;

fn main() {
    //* calling the run fn. of the print file
    // print::run();
    // vars::run();
    // types::run();
    //stringz::run();
    tuples::run();
}
