//* public fn. 
pub fn run() {
    println!("Hello from print.rs!!");

    // Positional Arguments
    println!("Position 0: {}, Pos 1: {}, Pos 3: {}, Pos 2 : {}", 0, 1, 3, 2);

    // Named Arguments
    println!("{name} likes to eat {fruit}", name = "Jadu", fruit = "Alu");

    // Placeholder Traits
    println!("Binary : {:b}, Hexadecimal : {:x}, Octal : {:o}", 10, 10, 10);

    // Placeholder Debug Traits
    println!("{:?}", (12, true, "Jaaadu"));

}