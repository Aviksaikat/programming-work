//? Primitive str = Immutable fixed-length string somewhere in memory
//? String = Growable, heap-allocated data structure - Use when you need to modify or own string data

pub fn run() {
    //* Primitive str
    // let string = "Hello" 

    //* heap allocated data 
    //* mut to make this string mutable
    let mut string = String::from("Hello");

    println!("String : {}", string);

    println!("Len : {}", string.len());

    //? push char
    string.push('W');
    println!("New String : {}", string);
    //? push string
    string.push_str(" World!");
    println!("After pushing 'World!' : {}", string);

    //? check capacity
    println!("Capacity : {}", string.capacity());

    //? if empty
    println!("Is Empty : {}", string.is_empty());

    //? if it contains something
    println!("Contains 'ellow' : {}", string.contains("elloW"));

    //? replace
    println!("Replaced : {}", string.replace("elloW", "ello"));

    //* look at the output here, it's call by value 
    for i in string.split_whitespace() {
        println!("{}", i);
    }

    //? string with size
    let mut s = String::with_capacity(5);
    s.push('a');
    s.push('b');

    //? Assertion testing (same like solidity)
    assert_eq!(2, s.len());
    assert_eq!(5, s.capacity());

    println!("{}", s);

}