fn main() {
    
    //* not mutable by default
    //* let arr name : [type; size]
    let arr1: [i32; 4] = [1, 2, 3, 4];
    let arr2 = [1, 2, 3, 4];
    
    println!("{}", arr1[3]);
}
