fn main() {

    let tup1: (i32, bool, char) = (1, true, 'A');

    let mut tup2 = (2, false, 'B');

    println!("tup1.1 : {}", tup1.1);
    println!("tup2.2 : {}", tup2.2);

    tup2.0 = 10;
    println!("tup1.0 : {}", tup1.0);

}
