import telegram
from telegram.ext import Updater, CommandHandler
from pydub import AudioSegment

# constants
BOT_TOKEN = "5914450808:AAHEjDSYatueGG5oyX630kswBDUxtCuRvaE"

# create an Updater object
updater = Updater(token=BOT_TOKEN, use_context=True)

def download(update, context):
    # get the YouTube video URL from the command arguments
    url = context.args[0]
    # download the YouTube video and convert it to MP3
    audio = AudioSegment.from_youtube(url)
    audio.export('song.mp3', format='mp3')
    # send the MP3 file to the user
    context.bot.send_audio(chat_id=update.effective_chat.id, audio=open('song.mp3', 'rb'))

# add the command handler to the dispatcher
updater.dispatcher.add_handler(CommandHandler('download', download))

# start the bot
updater.start_polling()
print("Running....")
